#version 130

uniform sampler3D volume;
uniform sampler1D gradient;
uniform sampler3D normals;

uniform int u_RenderMethod;
uniform vec3 u_viewVec;
uniform vec3 u_uVec;
uniform vec3 u_vVec;
uniform vec3 u_dimension;
uniform float u_shaded;

uniform float u_TF2DValue;
uniform float u_TF2DRadius;
uniform vec4 u_TF2DColor;
uniform vec2 u_TF2DRange;

out vec4 fragColor;

float getVoxel(vec3 coord) {
    if ( coord.x < 0 || coord.y < 0 || coord.z < 0 || coord.x > 1 || 
        coord.y > 1 || coord.z > 1 ) {
        return 0;
    }

    return texture3D( volume, coord ).r;
}

vec4 getNormal(vec3 coord) {
    if ( coord.x < 0 || coord.y < 0 || coord.z < 0 || coord.x > 1 || 
        coord.y > 1 || coord.z > 1 ) {
        return vec4(0);
    }

    return texture3D( normals, coord );
}

vec4 getTransferFuncValue( float value ) {
    return texture1D( gradient, value );
}

vec4 Slicer(vec3 rayStart) {
    float val = getVoxel( rayStart );

    return getTransferFuncValue( val );
}

vec4 MIPRender(vec3 rayStart, int resolution) {
    //Set up interval based on resolution
    int halfres = resolution / 2;
    float interval = 1.732/( resolution - 1 );

    //Keep track of maximum
    float max = 0;

    //Take samples
    for (int i = 0; i < resolution; i++) {
        float val = getVoxel( rayStart + u_viewVec * ( ( i - halfres ) * interval ) );

        if ( val > max ) {
            max = val;
        }
    }

    return getTransferFuncValue( max );
}

vec4 CompositeRender(vec3 rayStart, int resolution) {
    //Set up interval based on resolution
    int halfres = resolution / 2;
    //sqrt(3)/(resolution-1)
    float interval = 1.732/( resolution - 1 );

    vec3 oldColor = vec3(0);
    float opacity = 1;

    //Take samples
    for (int i = 0; i < resolution; i++) {
        float val = getVoxel( rayStart + u_viewVec * ( ( i - halfres ) * interval ) );

        vec4 color = getTransferFuncValue( val );

        oldColor = color.rgb * color.a + (1 - color.a)*oldColor;
        opacity = opacity * (1-color.a);
    }

    return vec4(oldColor, (1-opacity));
}

vec4 TF2DRender(vec3 rayStart, int resolution) {
    //Set up interval based on resolution
    int halfres = resolution / 2;
    //sqrt(3)/(resolution-1)
    float interval = 1.732/( resolution - 1 );

    float opacity = 0;
    vec3 color = vec3(0);

    vec3 lightDir = -u_viewVec;

    //Take samples
    for (int i = 0; i < resolution; i++) {
        vec3 voxelCoord = rayStart + u_viewVec * ( ( i - halfres ) * interval );
        float val = getVoxel( voxelCoord );
        vec4 gNormal = getNormal( voxelCoord );
        vec3 normal = gNormal.rgb * 2.0 - vec3(1.0);

        float fv = val;
        float fx = u_TF2DValue;

        vec3 sampleColor = u_TF2DColor.rgb;

        if(gNormal.a <= u_TF2DRange.y && gNormal.a >= u_TF2DRange.x){
            if ( abs(fx - fv) < 0.0001 &&  gNormal.a < 0.0001 ) {
                opacity = 1;
                color = sampleColor;
            } else if ( gNormal.a > 0 && fv >= fx - u_TF2DRadius * gNormal.a && fv <= fx + u_TF2DRadius * gNormal.a ) {
                opacity = 1;

                float newOpacity = 1 - (1/u_TF2DRadius) * abs((fv - fx)/gNormal.a);
                newOpacity = newOpacity * u_TF2DColor.a;

                float illumination = dot( normal, lightDir );
                vec3 newColor = u_shaded * (sampleColor * illumination 
                    + vec3(1) * pow( max(dot( reflect( normal, lightDir ), -lightDir ), 0),100)) + (1-u_shaded)*sampleColor ;
                color = newColor * newOpacity + (1 - newOpacity) * color;
            }
        }
    }

    return vec4( color, opacity );
}

void main(){
    //Set resolution
    int resolution = 500;

    //Calculate volume scales
    vec3 volumeScale = u_dimension/length(u_dimension);

    //Get pixel position, scale it and add the volume center
    vec3 rayStart = u_uVec * ( gl_TexCoord[0].x - 0.5 ) + u_vVec * ( gl_TexCoord[0].y - 0.5 );
    rayStart = rayStart / volumeScale;
    rayStart = vec3( 0.5 ) + rayStart;

    //Render 
    if ( u_RenderMethod == 1 ) {
        fragColor = MIPRender( rayStart, resolution );
    } else if ( u_RenderMethod == 2 ) {
        fragColor = CompositeRender( rayStart, resolution );
    } else if ( u_RenderMethod == 3 ) {
        fragColor = TF2DRender( rayStart, resolution );
    } else {
        fragColor = Slicer( rayStart );
    }
}
