/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package volume;

import util.Vector;

/**
 *
 * @author michel
 */
public class GradientVolume {

    public GradientVolume(Volume vol) {
        volume = vol;
        dimX = vol.getDimX();
        dimY = vol.getDimY();
        dimZ = vol.getDimZ();
        data = new float[dimX * dimY * dimZ * 3];
        compute();
        maxmag = -1.0;
    }

    public VoxelGradient getGradient(int x, int y, int z) {
        return new VoxelGradient( data[(x + dimX * (y + dimY * z))*3 + 0],
                data[(x + dimX * (y + dimY * z))*3 + 1],
                data[(x + dimX * (y + dimY * z))*3 + 2]);
    }

    
    public void setGradient(int x, int y, int z, float[] value) {
        data[(x + dimX * (y + dimY * z))*3] = value[0];
        data[(x + dimX * (y + dimY * z))*3 + 1] = value[1];
        data[(x + dimX * (y + dimY * z))*3 + 2] = value[2];
    }

    public void setVoxel(int i, VoxelGradient value) {
        data[i*3 + 0] = value.x;
        data[i*3 + 1] = value.y;
        data[i*3 + 2] = value.z;
    }

    public VoxelGradient getVoxel(int i) {
        return new VoxelGradient(data[i*3+0], data[i*3+1], data[i*3 + 2]);
    }

    public int getDimX() {
        return dimX;
    }

    public int getDimY() {
        return dimY;
    }

    public int getDimZ() {
        return dimZ;
    }
    
    private double getVoxel(int x, int y, int z){
        if (x < 0 || x > dimX-1 || y < 0 || y > dimY -1 || z < 0 || z > dimZ -1 ) {
            return 0;
        }
        
        return volume.getVoxel(x, y, z);
    }

    private void compute() {

        for (int z = 0; z < volume.getDimZ(); z++) {
            for (int y = 0; y < volume.getDimY(); y++) {
                for (int x = 0; x < volume.getDimX(); x++) {
                    float[] value = new float[]{
                            0.5f * (float)(getVoxel(x+1,y,z)-getVoxel(x-1,y,z)),
                            0.5f * (float)(getVoxel(x,y+1,z)-getVoxel(x,y-1,z)),
                            0.5f * (float)(getVoxel(x,y,z+1)-getVoxel(x,y,z-1))
                    };
                    setGradient( x, y, z, value );
                }
            }
        }
                
    }
    
    public double getMaxGradientMagnitude() {
        if (maxmag >= 0) {
            return maxmag;
        } else {
            double magnitude = new VoxelGradient(data[0],data[1],data[2]).mag;
            for (int i=0; i<data.length/3; i++) {
                double newMag = new VoxelGradient(data[i*3+0],data[i*3+1],data[i*3+2]).mag;
                magnitude = newMag > magnitude ? newMag : magnitude;
            }   
            maxmag = magnitude;
            return magnitude;
        }
    }
    
    private int dimX, dimY, dimZ;
    private VoxelGradient zero = new VoxelGradient();
    float[] data;
    Volume volume;
    double maxmag;
}
