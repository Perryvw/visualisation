/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package volvis;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import gui.RaycastRendererPanel;
import gui.TransferFunction2DEditor;
import gui.TransferFunctionEditor;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.swing.Timer;
import util.TFChangeListener;
import util.Vector;
import util.VectorMath;
import volume.GradientVolume;
import volume.Volume;
import volume.VoxelGradient;

/**
 *
 * @author michel
 */
public class RaycastRenderer extends Renderer implements TFChangeListener {

    public final static int RENDER_METHOD_SLICER = 0;
    public final static int RENDER_METHOD_MIP = 1;
    public final static int RENDER_METHOD_COMPOSITE = 2;
    public final static int RENDER_METHOD_TF2D = 3;

    private Volume volume = null;
    private GradientVolume gradients = null;
    RaycastRendererPanel panel;
    TransferFunction tFunc;
    TransferFunctionEditor tfEditor;
    TransferFunction2DEditor tfEditor2D;
    private int renderMethod = RENDER_METHOD_SLICER;
    /**
     * Resolution to render in when moving
     */
    private static final int MIN_RESOLUTION = 25;
    /**
     * Time to wait before update image to new resolution (in ms)
     */
    private static final int RESOLUTION_UPDATE_DELAY = 100;
    /**
     * Increase in resolution every update
     */
    private static final int RESOLUTION_INCREASE = 10;
    /**
     * Percentage of full resolution to render the volume in
     */
    private int resolutionPercentage = MIN_RESOLUTION;
    private Visualization visualization;
    private Timer timer;
    //GPU stuff
    private int volumeTexture;
    private int gradientTexture;
    private int normalTexture;
    private int shader;
    private GL2 gl;
    private boolean shaded = false;
    
    private long lastFrame = 0;

    public RaycastRenderer(Visualization visualization) {
        this.visualization = visualization;
        panel = new RaycastRendererPanel(this);
        panel.setSpeedLabel("0");
        
        lastFrame = System.currentTimeMillis();
    }

    private String readFile(File file) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(file));
            String string = "";
            String line;
            while ((line = in.readLine()) != null) {
                string += line + "\n";
            }
            return string;
        } catch (IOException ex) {
            Logger.getLogger(RaycastRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    @Override
    public void init(GL2 gl) {
        this.gl = gl;
        int[] textures = new int[3];
        gl.glGenTextures(3, textures, 0);
        volumeTexture = textures[0];
        gradientTexture = textures[1];
        normalTexture = textures[2];

        int vertex = gl.glCreateShader(GL2.GL_VERTEX_SHADER);
        int fragment = gl.glCreateShader(GL2.GL_FRAGMENT_SHADER);
        shader = gl.glCreateProgram();
        gl.glShaderSource(vertex, 1, new String[]{readFile(new File("shader.vertex"))}, null);
        gl.glCompileShader(vertex);

        gl.glShaderSource(fragment, 1, new String[]{readFile(new File("shader.fragment"))}, null);
        gl.glCompileShader(fragment);

        byte[] log = new byte[1000];
        gl.glGetShaderInfoLog(fragment, 1000, new int[]{1000}, 0, log, 0);
        System.out.println("Fragment shader: " + new String(log));

        gl.glAttachShader(shader, vertex);
        gl.glAttachShader(shader, fragment);
        gl.glLinkProgram(shader);
        gl.glValidateProgram(shader);
        log = new byte[1000];
        gl.glGetProgramInfoLog(fragment, 1000, new int[]{1000}, 0, log, 0);
        System.out.println("Program:" + new String(log));
    }

    private boolean generated = false;
    private boolean needGradientUpdate = true;

    public void create3DTexture() {
        if (generated) {
            return;
        }

        gl.glBindTexture(GL2.GL_TEXTURE_3D, volumeTexture);

        ByteBuffer buffer = ByteBuffer.allocateDirect((volume.getDimX() * volume.getDimY() * volume.getDimZ())  * 4)
                .order(ByteOrder.nativeOrder());
        float max = volume.getMaximum();
        for (int i = 0; i < volume.getDimZ(); i++) {
            for (int j = 0; j < volume.getDimY() ; j++) {
                for (int k = 0; k < volume.getDimX(); k++) {
                    buffer.put((byte) ((volume.getVoxel(k, j, i)/max) * 255));
                }
            }
        }
        buffer.rewind();

        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);
        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP);
        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_R, GL2.GL_CLAMP);

        gl.glTexImage3D(GL2.GL_TEXTURE_3D, 0, GL2.GL_RED, volume.getDimX(),
                volume.getDimY(), volume.getDimZ(), 0, GL2.GL_RED, GL.GL_UNSIGNED_BYTE, buffer);

        gl.glBindTexture(GL2.GL_TEXTURE_3D, 0);

        System.out.println("Generated 3D volume texture");
    }
    
    public void setShaded(boolean s ) {
        shaded = s;
        changed();
    }
    
    public void create3DNormalTexture() {
        if (generated) {
            return;
        }

        gl.glBindTexture(GL2.GL_TEXTURE_3D, normalTexture);

        ByteBuffer buffer = ByteBuffer.allocateDirect(volume.getDimX() * volume.getDimY() * volume.getDimZ() * 4)
                .order(ByteOrder.nativeOrder());
        double volumeMax = volume.getMaximum();
        for (int i = 0; i < volume.getDimZ(); i++) {
            for (int j = 0; j < volume.getDimY(); j++) {
                for (int k = 0; k < volume.getDimX(); k++) {
                    VoxelGradient gradient = gradients.getGradient(k, j, i);
                    gradient.x = (float)(0.5 * gradient.x/gradient.mag + 0.5);
                    gradient.y = (float)(0.5 * gradient.y/gradient.mag + 0.5);
                    gradient.z = (float)(0.5 * gradient.z/gradient.mag + 0.5);
                    buffer.put((byte) ( 255 * gradient.x ));
                    buffer.put((byte) ( 255 * gradient.y ));
                    buffer.put((byte) ( 255 * gradient.z ));
                    buffer.put((byte) ( 255 * gradient.mag / volumeMax ));
                }
            }
        }
        buffer.rewind();

        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);
        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP);
        gl.glTexParameteri(GL2.GL_TEXTURE_3D, GL2.GL_TEXTURE_WRAP_R, GL2.GL_CLAMP);

        gl.glTexImage3D(GL2.GL_TEXTURE_3D, 0, GL2.GL_RGBA, volume.getDimX(),
                volume.getDimY(), volume.getDimZ(), 0, GL2.GL_RGBA, GL.GL_UNSIGNED_BYTE, buffer);

        gl.glBindTexture(GL2.GL_TEXTURE_3D, 0);

        System.out.println("Generated 3D gradient texture");
    }

    private void createGradientTexture() {
        needGradientUpdate = false;
        gl.glActiveTexture(GL.GL_TEXTURE1);
        gl.glBindTexture(GL2.GL_TEXTURE_1D, gradientTexture);

        ByteBuffer buffer = ByteBuffer.allocateDirect(tFunc.LUT.length * 4)
                .order(ByteOrder.nativeOrder());

        for (int i = 0; i < tFunc.LUT.length; i++) {
            TFColor color = tFunc.LUT[i];
            buffer.put((byte) (color.r * 255));
            buffer.put((byte) (color.g * 255));
            buffer.put((byte) (color.b * 255));
            buffer.put((byte) (color.a * 255));
        }
        buffer.flip();

        gl.glTexParameteri(GL2.GL_TEXTURE_1D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
        gl.glTexParameteri(GL2.GL_TEXTURE_1D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
        //Important to prevent noise
        gl.glTexParameteri(GL2.GL_TEXTURE_1D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);

        gl.glTexImage1D(GL2.GL_TEXTURE_1D, 0, GL2.GL_RGBA, tFunc.LUT.length,
                0, GL2.GL_RGBA, GL.GL_UNSIGNED_BYTE, buffer);

        gl.glBindTexture(GL2.GL_TEXTURE_1D, 0);
        gl.glActiveTexture(GL.GL_TEXTURE0);

        System.out.println("Generated 1D gradient texture");
    }

    public void gpuRender(double[] viewMatrix) {
        if(this.needGradientUpdate){
          createGradientTexture();
        }
        create3DTexture();
        create3DNormalTexture();
        generated = true;
        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];
        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);
        
        Vector volumeCenter = new Vector(volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        gl.glDisable(GL2.GL_LIGHTING);
        gl.glEnable(GL.GL_BLEND);
        
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glEnable(GL2.GL_TEXTURE_3D);
        gl.glEnable(GL2.GL_TEXTURE_1D);

        gl.glActiveTexture(GL.GL_TEXTURE0);
        gl.glBindTexture(GL2.GL_TEXTURE_3D, volumeTexture);

        gl.glActiveTexture(GL.GL_TEXTURE1);
        gl.glBindTexture(GL2.GL_TEXTURE_1D, gradientTexture);
        
        gl.glActiveTexture(GL.GL_TEXTURE2);
        gl.glBindTexture(GL2.GL_TEXTURE_3D, normalTexture);

        gl.glActiveTexture(GL.GL_TEXTURE0);

        gl.glUseProgram(shader);

        gl.glUniform1i(gl.glGetUniformLocation(shader, "u_RenderMethod"),
                renderMethod);
        gl.glUniform3f(gl.glGetUniformLocation(shader, "u_viewVec"),
                (float) viewVec[0], (float) viewVec[1], (float) viewVec[2]);
        gl.glUniform3f(gl.glGetUniformLocation(shader, "u_uVec"),
                (float) uVec[0], (float) uVec[1], (float) uVec[2]);
        gl.glUniform3f(gl.glGetUniformLocation(shader, "u_vVec"),
                (float) vVec[0], (float) vVec[1], (float) vVec[2]);
        gl.glUniform3f(gl.glGetUniformLocation(shader, "u_dimension"),
                volume.getDimX(), volume.getDimY(), volume.getDimZ());
        gl.glUniform1f(gl.glGetUniformLocation(shader, "u_shaded"), shaded ? 1 : 0 );
        
        gl.glUniform1f(gl.glGetUniformLocation(shader, "u_TF2DValue"), (float)tfEditor2D.triangleWidget.baseIntensity/(float)volume.getMaximum() );
        gl.glUniform1f(gl.glGetUniformLocation(shader, "u_TF2DRadius"), (float)tfEditor2D.triangleWidget.radius );
        gl.glUniform4f(gl.glGetUniformLocation(shader, "u_TF2DColor"), 
                (float)tfEditor2D.triangleWidget.color.r, (float)tfEditor2D.triangleWidget.color.g, 
                (float)tfEditor2D.triangleWidget.color.b, (float)tfEditor2D.triangleWidget.color.a );
        gl.glUniform2f(gl.glGetUniformLocation(shader, "u_TF2DRange"),
                tfEditor2D.triangleWidget.minGradient,
                tfEditor2D.triangleWidget.maxGradient);
        
        gl.glUniform1i(gl.glGetUniformLocation(shader, "volume"), 0);
        gl.glUniform1i(gl.glGetUniformLocation(shader, "gradient"), 1);
        gl.glUniform1i(gl.glGetUniformLocation(shader, "normals"), 2);

        double halfWidth = image.getWidth() / 2.0;

        double max = Math.max(volume.getDimX(), Math.max(volume.getDimY(), volume.getDimZ()));
        double ratio = max / Math.min(volume.getDimX(), Math.min(volume.getDimY(), volume.getDimZ()));

        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glBegin(GL2.GL_QUADS);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glTexCoord2d(0, 0);
        gl.glVertex3d(-halfWidth, -halfWidth, 0.0);
        gl.glTexCoord2d(0, 1);
        gl.glVertex3d(-halfWidth, halfWidth, 0.0);
        gl.glTexCoord2d(1, 1);
        gl.glVertex3d(halfWidth, halfWidth, 0.0);
        gl.glTexCoord2d(1, 0);
        gl.glVertex3d(halfWidth, -halfWidth, 0.0);
        gl.glEnd();
        gl.glPopMatrix();

        gl.glUseProgram(0);
        gl.glBindTexture(GL2.GL_TEXTURE_3D, 0);
    }

    public void setRenderMethod(int method) {
        this.renderMethod = method;
        changed();
    }

    public void setVolume(Volume vol) {
        generated = false;
        
        volume = null;
        gradients = null;
        //Remove old textures
        gl.glDeleteTextures(3, new int[]{volumeTexture, gradientTexture, normalTexture}, 0);
        
        //Force garbage collection
        System.gc();
        
        System.out.println("Assigning volume");
        volume = vol;

        System.out.println("Computing gradients");

        gradients = new GradientVolume(vol);
        

        // set up image for storing the resulting rendering
        // the image width and height are equal to the length of the volume diagonal
        int imageSize = (int) Math.floor(Math.sqrt(vol.getDimX() * vol.getDimX() + vol.getDimY() * vol.getDimY()
                + vol.getDimZ() * vol.getDimZ()));
        if (imageSize % 2 != 0) {
            imageSize = imageSize + 1;
        }
        image = new BufferedImage(imageSize, imageSize, BufferedImage.TYPE_INT_ARGB);
        // create a standard TF where lowest intensity maps to black, the highest to white, and opacity increases
        // linearly from 0.0 to 1.0 over the intensity range
        tFunc = new TransferFunction(volume.getMinimum(), volume.getMaximum());

        // uncomment this to initialize the TF with good starting values for the orange dataset 
        //tFunc.setTestFunc();
        tFunc.addTFChangeListener(this);
        tfEditor = new TransferFunctionEditor(tFunc, volume.getHistogram());

        tfEditor2D = new TransferFunction2DEditor(volume, gradients);
        tfEditor2D.addTFChangeListener(this);

        System.out.println("Finished initialization of RaycastRenderer");
    }

    public RaycastRendererPanel getPanel() {
        return panel;
    }

    public TransferFunction2DEditor getTF2DPanel() {
        return tfEditor2D;
    }

    public TransferFunctionEditor getTFPanel() {
        return tfEditor;
    }

    short getVoxel(double[] coord) {

        if (coord[0] < 0 || coord[0] > volume.getDimX() || coord[1] < 0 || coord[1] > volume.getDimY()
                || coord[2] < 0 || coord[2] > volume.getDimZ()) {
            return 0;
        }

        int x = (int) Math.floor(coord[0]);
        int y = (int) Math.floor(coord[1]);
        int z = (int) Math.floor(coord[2]);

        return volume.getVoxel(x, y, z);
    }

    short getInterpolatedVoxel(double[] coord) {
        if (coord[0] < 0 || coord[0] > volume.getDimX() - 2 || coord[1] < 2 || coord[1] > volume.getDimY() - 2
                || coord[2] < 0 || coord[2] > volume.getDimZ() - 2) {
            return 0;
        }

        int minX = (int) Math.floor(coord[0]);
        int minY = (int) Math.floor(coord[1]);
        int minZ = (int) Math.floor(coord[2]);

        double dx = coord[0] - minX;
        double dy = coord[1] - minY;
        double dz = coord[2] - minZ;

        double c0 = volume.getVoxel(minX, minY, minZ) * (1 - dx) + volume.getVoxel(minX + 1, minY, minZ) * dx;
        double c1 = volume.getVoxel(minX, minY + 1, minZ) * (1 - dx) + volume.getVoxel(minX + 1, minY + 1, minZ) * dx;
        double c2 = volume.getVoxel(minX, minY, minZ + 1) * (1 - dx) + volume.getVoxel(minX + 1, minY, minZ + 1) * dx;
        double c3 = volume.getVoxel(minX, minY + 1, minZ + 1) * (1 - dx) + volume.getVoxel(minX + 1, minY + 1, minZ + 1) * dx;

        double c4 = c0 * (1 - dy) + c1 * dy;
        double c5 = c2 * (1 - dy) + c3 * dy;

        return (short) (c4 * (1 - dz) + c5 * dz);
    }

    @Override
    public void update() {
        
    }

    private void setImageColor(int x, int y, int w, int h, TFColor color) {
        for (int k = 0; k < h; k++) {
            for (int l = 0; l < w; l++) {
                // BufferedImage expects a pixel color packed as ARGB in an int
                int c_alpha = color.a <= 1.0 ? (int) Math.floor(color.a * 255) : 255;
                int c_red = color.r <= 1.0 ? (int) Math.floor(color.r * 255) : 255;
                int c_green = color.g <= 1.0 ? (int) Math.floor(color.g * 255) : 255;
                int c_blue = color.b <= 1.0 ? (int) Math.floor(color.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(x + k, y + l, pixelColor);
            }
        }
    }

    void slicer(double[] viewMatrix) {

        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];
        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();

        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                //pixelCoord = uVec * (i - imageCenter) + vVec * (i - imageCenter) + volumeCenter
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter)
                        + volumeCenter[0];
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter)
                        + volumeCenter[1];
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter)
                        + volumeCenter[2];

                int val = getInterpolatedVoxel(pixelCoord);

                // Map the intensity to a grey value by linear scaling
                //voxelColor.r = val/max;
                //voxelColor.g = voxelColor.r;
                //voxelColor.b = voxelColor.r;
                //voxelColor.a = val > 0 ? 1.0 : 0.0;  // this makes intensity 0 completely transparent and the rest opaque
                // Alternatively, apply the transfer function to obtain a color
                voxelColor = tFunc.getColor(val);

                // BufferedImage expects a pixel color packed as ARGB in an int
                int c_alpha = voxelColor.a <= 1.0 ? (int) Math.floor(voxelColor.a * 255) : 255;
                int c_red = voxelColor.r <= 1.0 ? (int) Math.floor(voxelColor.r * 255) : 255;
                int c_green = voxelColor.g <= 1.0 ? (int) Math.floor(voxelColor.g * 255) : 255;
                int c_blue = voxelColor.b <= 1.0 ? (int) Math.floor(voxelColor.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(i, j, pixelColor);
            }
        }
    }

    private void MIPRender(double[] viewMatrix) {
        int stepSizeX = (int) Math.floor(image.getWidth() / (image.getWidth() / 100.0f * (float) resolutionPercentage));
        int stepSizeY = (int) Math.floor(image.getHeight() / (image.getHeight() / 100.0f * (float) resolutionPercentage));

        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        Vector viewVec = new Vector(viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        Vector uVec = new Vector(viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        Vector vVec = new Vector(viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // image is square
        int imageCenter = image.getWidth() / 2;

        Vector volumeCenter = new Vector(volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);
        int resolution = 50;
        int halfres = resolution / 2;
        double interval = viewVec.scale(new Vector(volume.getDimX(), volume.getDimY(), volume.getDimZ())).length() / (resolution - 1);

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();

        for (int j = 0; j < image.getHeight() - stepSizeY + 1; j += stepSizeY) {
            for (int i = 0; i < image.getWidth() - stepSizeX + 1; i += stepSizeX) {
                int val = 0;

                for (int r = 0; r < resolution; r++) {
                    Vector sampleCoord = uVec.scale(i - imageCenter).add(vVec.scale(j - imageCenter)).add(
                            volumeCenter.minus(viewVec.scale((r - halfres) * interval)));

                    int newVal = getInterpolatedVoxel(sampleCoord.asArray());
                    if (newVal > val) {
                        val = newVal;
                    }
                }

                /* voxelColor.r = val/max;
                voxelColor.g = voxelColor.r;
                voxelColor.b = voxelColor.r;
                voxelColor.a = val/max > 0.0 ? 1.0 : 0.0; */
                voxelColor = tFunc.getColor(val);
                setImageColor(i, j, stepSizeX, stepSizeY, voxelColor);
            }
        }
    }

    private void CompositeRender(double[] viewMatrix) {
        int stepSizeX = (int) Math.floor(image.getWidth() / (image.getWidth() / 100.0f * (float) resolutionPercentage));
        int stepSizeY = (int) Math.floor(image.getHeight() / (image.getHeight() / 100.0f * (float) resolutionPercentage));

        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        Vector viewVec = new Vector(viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        Vector uVec = new Vector(viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        Vector vVec = new Vector(viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // image is square
        int imageCenter = image.getWidth() / 2;

        Vector volumeCenter = new Vector(volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);
        int resolution = 50;
        int halfres = resolution / 2;
        double interval = viewVec.scale(new Vector(volume.getDimX(), volume.getDimY(), volume.getDimZ())).length() / (resolution - 1);

        for (int j = 0; j < image.getHeight() - stepSizeY + 1; j += stepSizeY) {
            for (int i = 0; i < image.getWidth() - stepSizeX + 1; i += stepSizeX) {

                // sample on a plane through the origin of the volume data
                TFColor voxelColor = new TFColor();
                double cr = 0;
                double cg = 0;
                double cb = 0;
                double ca = 0;

                for (int r = 0; r < resolution; r++) {
                    Vector sampleCoord = uVec.scale(i - imageCenter).add(vVec.scale(j - imageCenter)).add(
                            volumeCenter.minus(viewVec.scale((r - halfres) * interval)));

                    int newVal = getInterpolatedVoxel(sampleCoord.asArray());
                    TFColor color = tFunc.getColor(newVal);

                    cr = cr + color.r * color.a;
                    cb = cb + color.b * color.a;
                    cg = cg + color.g * color.a;
                    ca = ca + color.a;

                    if ((cr > 1 && cb > 1 && cg > 1) || ca > 1.0) {
                        break;
                    }
                }
                voxelColor.a = ca;
                voxelColor.r = cr;
                voxelColor.g = cg;
                voxelColor.b = cb;
                setImageColor(i, j, stepSizeX, stepSizeY, voxelColor);
            }
        }
    }

    private void TF2DRender(double[] viewMatrix) {
        int stepSizeX = (int) Math.floor(image.getWidth() / (image.getWidth() / 100.0f * (float) resolutionPercentage));
        int stepSizeY = (int) Math.floor(image.getHeight() / (image.getHeight() / 100.0f * (float) resolutionPercentage));

        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        Vector viewVec = new Vector(viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        Vector uVec = new Vector(viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        Vector vVec = new Vector(viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // image is square
        int imageCenter = image.getWidth() / 2;

        Vector volumeCenter = new Vector(volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);
        int resolution = 50;
        int halfres = resolution / 2;
        double interval = viewVec.scale(new Vector(volume.getDimX(), volume.getDimY(), volume.getDimZ())).length() / (resolution - 1);

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        
        double value = tfEditor2D.triangleWidget.baseIntensity;
        double radius = tfEditor2D.triangleWidget.radius;
        TFColor color = tfEditor2D.triangleWidget.color;
        Vector light = new Vector(1,1,1);

        for (int j = 0; j < image.getHeight() - stepSizeY + 1; j += stepSizeY) {
            for (int i = 0; i < image.getWidth() - stepSizeX + 1; i += stepSizeX) {

                for (int r = 0; r < resolution; r++) {
                    // volumeCenter + uVec * (i - imageCenter) + vVec * (j - imageCenter) + viewVec * ( r - halfres ) * interval
                    Vector sampleCoord = uVec.scale(i - imageCenter).add(vVec.scale(j - imageCenter)).add(
                            volumeCenter.minus(viewVec.scale((r - halfres) * interval)));

                    int newVal = getInterpolatedVoxel(sampleCoord.asArray());
                    Vector gradient = new Vector();
                    
                    double vlength = gradient.length();
                    
                    if ( vlength > 0 && value - radius * vlength <= newVal && newVal <= value + radius * vlength ){
                        double opac = 1 - (1/radius) * Math.abs((value - newVal)/vlength);
                        //TFColor newColor = new TFColor( color.r, color.g, color.b, opac );
                        Vector orgColor = new Vector( color.r, color.g, color.b );
                        Vector black = new Vector();
                        
                        double illum = gradient.normalized().dot(light.normalized());
                        
                        Vector blendColor = orgColor.scale( illum );
                        
                        setImageColor(i, j, stepSizeX, stepSizeY, new TFColor( blendColor.x, blendColor.y, blendColor.z, 1 ));
                        break;
                    } else {
                        
                    }
                }                
            }
        }

    }

    private void drawBoundingBox(GL2 gl) {
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glColor4d(1.0, 1.0, 1.0, 1.0);
        gl.glLineWidth(1.5f);
        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glDisable(GL.GL_LINE_SMOOTH);
        gl.glDisable(GL.GL_BLEND);
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glPopAttrib();
    }

    @Override
    public void visualize(GL2 gl) {
        this.gl = gl;
        if (volume == null) {
            return;
        }

        drawBoundingBox(gl);

        gl.glGetDoublev(GL2.GL_MODELVIEW_MATRIX, viewMatrix, 0);

        gpuRender( viewMatrix );

        long endTime = System.currentTimeMillis();
        double runningTime = (endTime - lastFrame);
        lastFrame = endTime;
        panel.setSpeedLabel(Double.toString(runningTime));

        Texture texture = AWTTextureIO.newTexture(gl.getGLProfile(), image, false);

        gl.glPushAttrib(GL2.GL_LIGHTING_BIT);
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        // draw rendered image as a billboard texture
        texture.enable(gl);
        texture.bind(gl);
        double halfWidth = image.getWidth() / 2.0;
        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glBegin(GL2.GL_QUADS);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glTexCoord2d(0.0, 0.0);
        gl.glVertex3d(-halfWidth, -halfWidth, 0.0);
        gl.glTexCoord2d(0.0, 1.0);
        gl.glVertex3d(-halfWidth, halfWidth, 0.0);
        gl.glTexCoord2d(1.0, 1.0);
        gl.glVertex3d(halfWidth, halfWidth, 0.0);
        gl.glTexCoord2d(1.0, 0.0);
        gl.glVertex3d(halfWidth, -halfWidth, 0.0);
        gl.glEnd();
        texture.disable(gl);
        texture.destroy(gl);
        gl.glPopMatrix();

        gl.glPopAttrib();

        int error;
        if ((error = gl.glGetError()) > 0) {
            System.out.println("some OpenGL error: " + error);
        }
    }
    private BufferedImage image;
    private double[] viewMatrix = new double[4 * 4];

    @Override
    public void changed() {
        needGradientUpdate = true;
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).changed();
        }
    }
}
