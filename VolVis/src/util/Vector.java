package util;

/**
 * Vector math implementation because VectorMath.java is too annoying.
 * Open source, free to use.
 * 
 * Author: Perry van Wesel
 */
public class Vector {
    
    public double x = 0;
    public double y = 0;
    public double z = 0;
    
    public Vector(){
    }
    
    public Vector(double x, double y){
        this.x = x;
        this.y = y;
    }
    
    public Vector(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public double length() {
        return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
    }
    
    public Vector add(Vector v2){
        return new Vector(this.x + v2.x, this.y + v2.y, this.z + v2.z);
    }
    
    public Vector minus(Vector v2){
        return new Vector(this.x - v2.x, this.y - v2.y, this.z - v2.z);
    }
    
    public Vector scale(double s) {
        return new Vector(this.x * s, this.y * s, this.z * s);
    }
    
    public Vector scale(Vector v2) {
        return new Vector(this.x * v2.x, this.y * v2.y, this.z * v2.z);
    }
    
    public Vector scaleTo(double l){
        double length = this.length();
        if (length > 0) {
            return this.scale(l/length);
        } else {
            return new Vector();
        }
    }
    
    public Vector normalized() {
        double length = this.length();
        if (length > 0) {
            return this.scale(1/length);
        } else {
            return new Vector();
        }
    }
    
    public double dot(Vector v2) {
        return this.x * v2.x + this.y * v2.y + this.z * v2.z;
    }
    
    public Vector cross(Vector v2) {
        return new Vector(
            this.y * v2.z - this.z * v2.y,
            this.z * v2.x - this.x * v2.z,
            this.x * v2.y - this.y * v2.x 
	);
    }
    
    public double[] asArray() {
        return new double[]{this.x, this.y, this.z};
    }
    
    @Override
    public String toString() {
        return "Vector("+this.x+","+this.y+","+this.z+")";
    }
}
