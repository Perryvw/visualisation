\documentclass[a4paper,twoside,11pt]{article}
\usepackage{a4wide,graphicx,fancyhdr,amsmath,amssymb}
\usepackage{pdfpages}
\usepackage{subcaption}
\usepackage{float}
\usepackage{multicol}
\usepackage{hyperref}

%----------------------- Macros and Definitions --------------------------

\setlength\headheight{20pt}
\addtolength\topmargin{-10pt}
\addtolength\footskip{20pt}

\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\figref}[1]{Figure \ref{#1}}
\newcommand{\R}{{\mathbb R}}
\newcommand{\dir}{\vec{d}}
\newcommand{\pol}{{\cal P}}
\newcommand{\robot}{{\cal R}}
\newcommand{\triang}{{\cal T}}
\newcommand{\dist}{\mathrm{dist}}
\newcommand{\eps}{\varepsilon}
\newcommand{\Mid}{:}
\newcommand{\ch}{\mathcal{CH}}
\newcommand{\exercise}[1]{\noindent{\bf Exercise #1:\\}}
\newcommand{\challenge}{\noindent{\bf Challenge problem:}}
\newenvironment{solution}{\begingroup\Solenv}{\END\endgroup}
\def\Solenv{\vspace{.5\baselineskip}\penalty100\advance\leftskip by\parindent
  \advance\leftmargini by\parindent
  \Sol\ \ignorespaces}
\def\Sol{\noindent{\bf Solution\/:}\nobreak}
\def\END{\unskip~\medbreak\medbreak\medbreak}
\def\qed{\hfill$\Box$}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}

\fancypagestyle{plain}{%
\fancyhf{}
\fancyhead[LO,RE]{\sffamily technische universiteit eindhoven}
\fancyhead[RO,LE]{\sffamily 2IMV20 Visualization}
\fancyfoot[LO,RE]{\sffamily /department of computer science}
\fancyfoot[RO,LE]{\sffamily\bfseries\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[RO,LE]{\sffamily 2IMV20 Visualization}
\fancyhead[LO,RE]{\sffamily technische universiteit eindhoven}
\fancyfoot[LO,RE]{\sffamily /department of computer science}
\fancyfoot[RO,LE]{\sffamily\bfseries\thepage}
\renewcommand{\headrulewidth}{1pt}
\renewcommand{\footrulewidth}{0pt}

%-------------------------------- Title ----------------------------------

\title{
Assignment 1
\vspace{5mm}
\hrule\vspace{1mm}
\textbf{\Huge Volume rendering}\\
\vspace{1mm}
\hrule
\vspace{5mm}
2IMV20 - Visualization\\
\today\\
\vspace{2cm}
Roy van den Hurk - 0817761\\
Perry van Wesel - 0818131\\
}
%\author{YOUR NAME}
\date{}

%--------------------------------- Text ----------------------------------

\begin{document}
\maketitle
\newpage

\tableofcontents
\newpage

\section{Introduction}
This report discusses the development of a ray caster used to visualise volume data. Ray casting is a way to render 3D volume data by casting a ray through the data for every pixel and determine the color of this pixel by processing samples of the data taken along the ray. In this case a specific instance of ray casting is used, namely volumetric ray casting. The difference between regular and volumetric ray casting is that in volumetric ray casting the ray does not stop when a surface is hit. Instead it passes through the volume and samples the volume at certain intervals. This allows the viewer to easily take a look into a 3D volume. This means that 3D primitives such as sphere and planes cannot easily be visualized. Instead, a data set is needed that provides information for the entire volume instead of just the surface. Such a volume can be created by using for example a CT scanner, which will scan an object and turn it into slices, these slices are later combined into a 3D volume. Looking at for example an MRI scan of a human, volumetric ray casting can visualize this in several ways: from visualizing just the skin to providing a clear image of the bone structure or even showing internal organs. 

\section{Ray casting}

\subsection{The ray casting mechanic}
Visualizing volume data is not as trivial as rendering standard geometry. While regular geometry exists of vertices and triangles that can directly be drawn, volume data is essentially just a 3D matrix with intensity values. Instead of rendering faces, this project uses the ray casting approach to visualise the volume data. This means that for each pixel in the viewport a ray is cast in the direction of the volume data, and samples are taken at some interval depending on the number of samples, also referred to as resolution. In reality any rays obviously have an infinite amount of samples, but that would take too long to calculate. Less samples mean less work, but it also increases the inaccuracy of the projection. This means a trade-off has to be made between performance and accuracy. For this project a resolution of 500 was used to obtain all results. This showed to give sufficiently accurate results while keeping the calculation time within reasonable bounds.\\
\\
So how can these samples be found? The ray caster in this project considers a camera with $\vec{u}$ representing the vector pointing right of the camera and $\vec{v}$ representing the vector pointing up from the camera. Furthermore the camera has a $\vec{view}$ vector that represent the direction the camera is pointed at. Since $\vec{u}$ and $\vec{v}$ are unit vectors in the same space as the viewport pixel coordinates, an offset for each pixel can be calculated for a pixel with coordinates $(x_p,y_p)$ in the following manner ( the coordinates of the center of the volume is subtracted since we want that to be in the center of the view ) :
$$ \text{pixelOffset} = (x,y) = \vec{u} * ( x_p - x_{center} ) + \vec{v} * ( y_p - y_{center} ) $$

Next, a sample $s_i$ can be found for resolution $r$ using the following formula:
$$ interval = \frac{\sqrt{volumeWidth^2 + volumeDepth^2 + volumeHeight^2}}{r-1} $$
$$ s_i = (x_{center}, y_{center}) + \text{pixelOffset} + \vec{view} * interval * ( i - r/2 ) $$
The interval is calculated by dividing the diagonal of the volume by the resolution minus 1. This ensures that samples are taken at most from one end of the diagonal of the volume data to the other end. The value for a sample is just calculating by adding the offset of the pixel to the center of the volume, and then adding a distance exactly towards or away from the camera depending on the sample number. Figure \ref{fig:raycast} shows how this math can be interpreted visually.

\begin{figure}[h]
\centering
\includegraphics[scale=0.7]{raycast2}
\caption{A visual representation of ray casting math.}
\label{fig:raycast}
\end{figure}

\subsection{Transfer functions}
When inspecting data with the ray caster it could be the case not all data is interesting. Therefore this project supports the use of a transfer function. This transfer function is a mapping from the volume data to a color and opacity. By default this mapping maps the lowest value to a transparent black and the highest value to a fully opaque white. Any other value is just interpolated between these values. Using this transfer function it is possible to assign high values only to values within a certain range or multiple ranges. This allows an analyst looking at the data to hide certain data or to highlight certain data ranges in the data. Finding a suitable transfer function is not an exact science, but depends more on experimentation. One approach that can work is to map values that occur often to high values and the rest to low values. Figure \ref{fig:transfer-func} shows the difference a transfer function can make on the same scene.

\begin{figure}[H]
\centering
\includegraphics[width=0.49\textwidth]{fish-tf1}
\includegraphics[width=0.49\textwidth]{fish-tf2}
\caption{The same scene with different transfer functions.}
\label{fig:transfer-func}
\end{figure}

%Comment out for now -conflict
\iffalse
As previously mentioned ray casting works by shooting a ray into an environment. Across this ray a number of samples are taken. Using an interpolation method, as will be explained in the trilinear interpolation section, these samples are transformed into scalar values. A transfer function will than be used to combine all samples along a single ray. One can take for example the maximum value or combine them in some way to extract interesting features from a data set. To determine the direction of a ray $R$ originating from pixel $p$ the view vector is needed. The view vector is a vector representing the direction that that camera is looking. When  the horizontal and vertical vectors describing the viewing plane are known this view vector can easily be calculated by taking the cross product between these vectors. When $R$ is known one simply has to take samples between the end $q_1$ and the beginning $q_0$ of $R$. For sample $q_t$ this is expressed with the following formula: \\

$q_t = q_0 + t * (q_1 - q_0)$ for $ t \in [0, 1]$. See \figref{fig:raycast}.

\begin{figure}[!Htb]
  \center
  \includegraphics[width=0.5\textwidth]{raycast}
  \caption{Ray casting}
  \label{fig:raycast}
Source: Visualization slides
\end{figure}

\begin{figure}[!Htb]
  \center
  \includegraphics[width=0.8\textwidth]{volume_ray_casting}
  \caption{Volumetric ray casting}
  \label{fig:volraycast}
\center
Source: \url{https://en.wikipedia.org/wiki/Volume_ray_casting}
\end{figure}

\begin{figure}
\begin{minipage}[b]{0.5\textwidth}
  \center
  \includegraphics[width=0.8\textwidth]{bilerp}
  \caption{Bilinear interpolation}
  \label{fig:bilerp}
\end{minipage}
\begin{minipage}[b]{0.5\textwidth}
  \center
  \includegraphics[width=0.8\textwidth]{trilerp}
  \caption{Trilinear interpolation}
  \label{fig:trilerp}
\end{minipage}
\center
Source: \url{https://en.wikipedia.org/wiki/Trilinear_interpolation}
\end{figure}
\fi

\subsection{Trilinear interpolation}
Linear interpolation is used to determine the value at point between any data points, using nearby points for which the value is known. See for example figure \figref{fig:bilerp}. $C_{00}, C_{10}, C_{01}, C_{11}$ are the known values and $C$ is the value to be determined. An easy way to determine this value is to do linear interpolation first on one axis followed by linear interpolation on the other axis. This can be expanded to higher dimensions by applying interpolation on all dimensions, which in 3D looks like depicted in figure \ref{fig:trilerp}.

\begin{figure}[h]
\begin{minipage}[b]{0.5\textwidth}
  \center
  \includegraphics[width=0.6\textwidth]{bilerp}
  \caption{Bilinear interpolation}
  \label{fig:bilerp}
\end{minipage}
\begin{minipage}[b]{0.5\textwidth}
  \center
  \includegraphics[width=0.6\textwidth]{trilerp}
  \caption{Trilinear interpolation}
  \label{fig:trilerp}
\end{minipage}
\center
Source: \url{https://en.wikipedia.org/wiki/Trilinear_interpolation}
\end{figure}

For ray casting in 3D, linear interpolation on three dimensions is used, also known a trilinear interpolation. The formula for this is as follows: \\
Let $(x, y, z)$ be the sample position, then the following variables can be defined.  \\
\begin{table}[!htb]
\centering
\begin{tabular}{lll}
$x_0 = \floor{x}$ & $x_1 = \ceil{x}$ & $\Delta x = {(x - x_1)\over x_1 - x_0}$  \\
$y_0 = \floor{y}$ & $y_1 = \ceil{y}$ & $\Delta y = {(y - y_1)\over y_1 - y_0}$  \\
$z_0 = \floor{z}$ & $z_1 = \ceil{z}$ & $\Delta z = {(z - z_1)\over z_1 - z_0}$  \\
\end{tabular}
\end{table}

\textit{Note: It is assumed that the values integer locations are known.}\\\\
The final value can than be calculated using the following formulas \\\\
$
c0 = (x_0, y_0, z_0) * (1 - dx) + (x_1, y_0, z_0) * \Delta x \\
c1 = (x_0, y_1, z_0) * (1 - dx) + (x_1, y_1, z_0) * \Delta x \\
c2 = (x_0, y_0, z_1) * (1 - dx) + (x_1, y_0, z_1) * \Delta x \\
c3 = (x_0, y_1, z_1) * (1 - dx) + (x_1, y_1, z_1) * \Delta x \\
\\
c4 = c0 * (1 - dy) + c1 * \Delta y \\
c5 = c2 * (1 - dy) + c3 * \Delta y \\
\\
\text{value} = c4 * (1 - dz) + c5 * \Delta z$\\

One of the reasons that trilinear interpolation is used, is to create a higher quality image. \figref{fig:pig_tri} Shows a pig ray cast using trilinear interpolation. \figref{fig:pig_nrs} Shows the same pig rendered with the exact same settings, however no trilinear interpolation is used. This is also known as nearest neighbour interpolation, in which simply the closest known value is taken.

\begin{figure}[h]
\begin{minipage}[b]{0.5\textwidth}
  \center
  \includegraphics[width=\linewidth]{pig_tri}
  \caption{Render using trilinear interpolation}
  \label{fig:pig_tri}
\end{minipage}
\begin{minipage}[b]{0.5\textwidth}
  \center
  \includegraphics[width=\linewidth]{pig_nrs}
  \caption{Render using nearest-neighbour}
  \label{fig:pig_nrs}
\end{minipage}
\end{figure}

\subsection{Responsiveness}

Ray casting requires far more computation time than more traditional render methods such as rasterisation. For small models in the range of $250^3$ voxels it is already very hard to visualize these models in real time. This causes ray casters to often respond very slowly to user interaction. One way to solve this problem is to ray cast at a lower resolution. Meaning that instead of for every pixel, for every group of pixels a ray is cast. This leads to a better performing application, however also leads to blocky images, see \figref{fig:lowrespig}. 

\begin{figure}[H]  
  \centering
  \includegraphics[scale=0.4]{lowrespig}
  \captionof{figure}{Ray-casting with a lower resolution}
  \label{fig:lowrespig}
\end{figure}

Another way to make a ray caster more responsive is to do the render using the GPU. A computer's GPU is optimized for vector and matrix multiplications, which are the main operations done during ray casting. Ray casting on the GPU can be done in a similar fashion to the CPU, rays are still cast through a volume for every pixel. However, some minor modifications have to be made. For example, the volume and gradient information has to be uploaded to the GPU in the form of a 3D texture, as the GPU cannot simply access the same data as the CPU version. Furthermore, it is important to note that when doing the ray casting in a fragment shader that all coordinates should be normalized. That is, map all coordinates to the range [0, 1] by dividing a coordinate by the maximum coordinate.\\
\\
An important drawback to notice is that this method requires the data to be uploaded to the GPU's graphic memory. When working with very large datasets, this could pose a problem. For all datasets considered for this project, any modern GPU should be able to handle this amount of data.

\subsection{Maximum intensity projection}
The simplest method to calculate a the color of a pixel is to simply take the highest value sample found along the view ray. This means the value of a pixel with $n$ samples with value $v_i$ for sample $i$ is simply the following formula:
$$c = f_t( max_{1 \leq i \leq n}(v_i))$$

Using the maximum value as input for the transfer function still allows to show different types of ranges of values.

\subsubsection{Result}
This render method gives a clear view of high values in the data set. This means that if an analyst knows that the highest values in a dataset will be clearly contrasted to other values, this method will give a clear image. Figure \ref{fig:tomato-fish-mip} shows the carp dataset that works well for MIP, since bones are clearly contrasted to the rest of the fish. Figure \ref{fig:tomato-fish-mip} shows the tomato dataset that does not have a lot of contrast in it. It is clear that for this type of data the MIP method is not very suitable

\begin{figure}[h]
	\center
	\includegraphics[width=0.4\textwidth]{fish_mip}
	\includegraphics[width=0.4\textwidth]{tomato-mip}
	\caption{A carp rendered using MIP on the left and tomato using MIP on the right.}
	\label{fig:tomato-fish-mip}
\end{figure}

\subsection{Compositing}

\label{compositing}
Compositing the different samples along the view ray aims to give a more or less realistic picture of the data, by using the fact that opacity of some sample means that the color of the sample behind it shines through. Considering opacities in the range from 0 to 1 where 0 is completely transparent and 1 is totally opaque, that means that if a sample has an opacity of 0.8, 20\% of the actual color at that point consists of the color of the point behind it. More formally: When sampling from back to front, the $(r,g,b)$ color $c_i$ of sample $i$ is defined as: 
$$c_i = s_i * a_i + c_{i-1}*(1-a_i)$$
Here $s_i$ is the sample color obtained from the transfer function at sample $i$ and $a_i$ is the opacity as returned by the sample function at sample $i$. In this formula $c_{i-1}$ represents the color calculated at the previous sample. This means that this sampling only produces correct results in a back-to-front order. In addition to this, the method in this implementation also calculates the total opacity $a$ of all samples along a ray according to the formula:
$$ a = 1-\prod\limits_{i=1}^{i}(1-a_i) $$
This means the final color of a ray with $n$ samples is the last $c_n$ calculated with opacity $a$.
\subsubsection{Results}
The main advantage of this render method is that the result is displayed roughly like it would look in reality, depending on the transfer function. This method also gives a good way to combine results from gradient-based opacity weighting, as will be discussed later in section \ref{gradbased-opacity-weighting}.

\begin{figure}[H]
	\center
	\includegraphics[scale=0.5]{carp-composite}
	\caption{The carp dataset using the composite render method.}
	\label{fig:carp-composite}
\end{figure}

\begin{figure}[H]
\center
\includegraphics[width=0.30\linewidth]{orange_peel}
  \includegraphics[width=0.30\linewidth]{pig_coins}
  \caption{The Pig and orange dataset using the composite render method.}
  \label{fig:orange-pig}
\end{figure}

As visible in figure \ref{fig:carp-composite}, this method is good to show depth or thickness of material (for example visible when comparing the tail with the body of the carp). It also allows shallow elements to show through the outer skin of the object, based on the opacity given to that skin in the transfer function. In the figure this is visible as the bones can be seen through the skin of the fish. Bones located closer to the skin are easier to see as there is less material preventing light to pass through, than with bones deeper in the body, where there is so much material between the bones and the skin that it blocks vision of the bones. Another example can be seen in figure \ref{fig:orange-pig}, which shows the inside of an orange through the (much thicker) peel as well as coins inside a pig.\\

The fact that using this method volumes of material can obscure vision of what is behind them can be a disadvantage sometimes, section \ref{gradbased-opacity-weighting} explains how to deal with this.

\section{2-D transfer function}

\subsection{Gradient-based opacity weighting}
\label{gradbased-opacity-weighting}
Interesting areas in the dataset are the boundaries between different components. These boundaries have to be found and visualised in some way. This is done using a gradient vector $\nabla f$. This gradient vector is calculated in the following way:\\
$$ \nabla f(x,y,z) = \left(
\begin{split}
\frac{1}{2}(f_{x+1,y,z} - f_{x-1,y,z}),\\
\frac{1}{2}(f_{x,y+1,z} - f_{x,y-1,z}),\\
\frac{1}{2}(f_{x,y,z+1} - f_{x,y,z-1})
\end{split}
\right) $$

This gradient vector definition means that the length of $\nabla f$ indicates how much the sides of the voxel differ in value. Boundaries are on transitions between different types of areas with some value, so the greater the length of the gradient vector $|\nabla f|$, the more obvious it is that a boundary is located at that point.

This gradient vector can be used to calculate the opacity $a_i$ of a sample at $x$, $y$ and $z$, in combination with a radius and threshold value as explained by Levoy\cite{Levoy}. The idea is to only look at values around some threshold value. For this the radius $r$ and threshold value $f_v$ are introduced. The formula for $a_i$ can be formulated as follows:
$$
a_i = a(x,y,z) = \begin{cases}
	1 & \text{if } |\nabla f(x,y,z)|=0 \text{ and } f(x,y,z) = f_v\\
	1-\dfrac{1}{r}\left|\dfrac{f_v - f(x,y,z)}{|\nabla f(x,y,z)|}\right| & \begin{split}&\text{if } |\nabla f(x,y,z)|>0 \text{ and }\\
	&f(x,y,z) - r|\nabla f(x,y,z)| \leq f_v \leq f(x,y,z) + r|\nabla f(x,y,z)|\end{split}\\
	0 & \text{otherwise}
\end{cases}
$$

Now that a good method is available to assign higher opacities only to interesting areas near boundaries, these different opacities still have to be combined along each ray. To combine these samples in a nice way the compositing method as explained in section \ref{compositing} can be used. As seen in the results of the compositing method, it can fail when there are volumes of the same material obscuring vision of anything interesting behind it. Using the opacity function as explained in this section, the samples in these volumes will not be visible, as the length of their gradient vector will be very small. This means that this method is good at displaying different interesting areas at the same time.

\subsection{2D transfer function}
When inspecting datasets, the boundaries between different materials are interesting. Therefore a 2D transfer function is introduced, defining a radius $r$ and a threshold intensity $f_v$, as explained by Levoy \cite{Levoy}. The threshold and radius are represented visually as a triangle over a plot of gradient vector lengths versus voxel values. The areas that are interesting are mostly the top of arcs that naturally appear, as that means there are high length gradient vectors there. The greater the length of a gradient vector the stronger the transition at that point, indicating a boundary. 

\subsection{Lighting}
An additional benefit if calculating gradient vectors is that they can be used as an approximation of the normal vector of a surface. This means that the color of a sample can be modified to include lighting information. For this project a simplified Phong model was used. This model considers ambient, diffuse and specular light. Ambient light is light that exists on each surface, in this case the ambient was omitted. Diffuse light illuminates surfaces facing the light source, this orientation relative to the light source can be calculated using the approximated normal. The diffuse light source is the same color as configured in the 2D transfer function editor.
 Finally there is specular lighting, which imitates reflections on surfaces. For this project the specular light was white. The normal vector $\hat{u}_i$ of sample $i$ can be calculated by just normalizing the gradient vector at $i$:
$$ \hat{u}_i =  \frac{\nabla f_i}{|\nabla f_i|} $$
To calculate the final color of a voxel this normal vector $\hat{u}$ is needed, as well as light direction $\ell$. The ray caster described here imitates light coming from the camera therefore $\ell = -\vec{view}$. The view vector is negated since the view vector is from camera to voxel, and the Phong model requires the vector to point the other way for its dot products.\\
The final color of a sample $i$ with normal vector $\vec{u}_i$ is calculated in the following way:
$$ c_i =  c_{base} * (\hat{u}_i \cdot \ell) + c_{white} * ( \hat{r}_i \cdot \ell )^\alpha $$
The first part of this formula calculates the diffuse part of the color by multiplying the dot product of the surface normal and the vector from the sample to the light source with the base color $c_{base}$ as was chosen in the 2D transfer function editor. The second part multiplies a white color with the $\alpha$-th power of the dot product between $\hat{r}_i$, which is the light vector as reflected on the surface of the sample:
$$ \hat{r}_i = 2(\ell \cdot \hat{u}_i) * \hat{u}_i - \ell $$
The $\alpha$ parameter determines the influence of the reflection on the specular light, metallic items usually have a higher $\alpha$ while matte materials have lower $\alpha$ values. For this project a fixed $\alpha = 100$ was used.

\subsubsection{Results}
Figure \ref{fig:carp-tf2d} shows the carp dataset rendered using gradient based opacity weighting as explained in the previous section. Comparing figure \ref{fig:carp-tf2d} to figure \ref{fig:carp-composite}, it is clear to see that the gradient based opacity weighting method shows a lot more information.

\begin{figure}[h]
\center
\includegraphics[scale=0.5]{carp-tf2d}
\caption{The carp dataset rendered using gradient-based opacity weighting.}
\label{fig:carp-tf2d}
\end{figure}

Enabling shading produces nice results allowing to see detailed surface shapes. Figure \ref{fig:shading} shows a comparison between the pig dataset as rendered using the 2D transfer function method without shading, the 2D transfer function with shading and the compositing method. Disabling shading gives a more or less solid looking shape of the pig, not showing any details. Enabling shading allows the viewer to see much more detail on the surface of the object, such as the fact that the flowers are slightly lower on the surface than the rest. Moving the object around to shift the white highlight allows to see even more data. This shaded model can be compared to the result of the compositing method. The results look roughly similar, however the difference is the fact that it is harder to see details like embossing on the surface of the object. Also the absence of the highlight does remove some of the detail, as rotating around the object does not change the way it looks, which might give some more insight on the surface of the object.

\begin{figure}[H]
\center
\includegraphics[width=0.3\textwidth]{pig-solid}
\includegraphics[width=0.3\textwidth]{pig-shaded}
\includegraphics[width=0.3\textwidth]{pig-composite}
\caption{The pig dataset rendered using the 2D transfer function with and without shading and the compositing method}
\label{fig:shading}
\end{figure}

\subsection{Triangle widget extension}
As discussed in the 2-D transfer function section a radius and threshold value is used to determine the opacity of a sample. Kniss\cite{Kniss} introduces two extra parameters, namely a minimum and a maximum value for the gradient magnitude in the form of two sliders. This allows for a more specific selection of values that will be taken into account. In \figref{fig:triangle-before} it can be seen that the entire radius of the triangle widget is used. \figref{fig:triangle-after} on the other hand shows that the extended triangle widget allows the selection of a small subsection.

\begin{figure}[H]
\begin{minipage}[b]{0.5\textwidth}
\center
\includegraphics[width=1\textwidth]{triangle_extension_before}
\caption{Basic triangle widget}
\label{fig:triangle-before}
\end{minipage}
\begin{minipage}[b]{0.5\textwidth}
\center
\includegraphics[width=1\textwidth]{triangle_extension_after}
\caption{Extended triangle widget}
\label{fig:triangle-after}
\end{minipage}
\end{figure}

\bibliography{bib}
\bibliographystyle{plain}
\end{document}