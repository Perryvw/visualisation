\documentclass[a4paper,twoside,11pt]{article}
\usepackage{a4wide,graphicx,fancyhdr,amsmath,amssymb}
\usepackage{pdfpages}
\usepackage{subcaption}
\usepackage{float}
\usepackage{multicol}
\usepackage{hyperref}

%----------------------- Macros and Definitions --------------------------

\setlength\headheight{20pt}
\addtolength\topmargin{-10pt}
\addtolength\footskip{20pt}

\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\figref}[1]{Figure \ref{#1}}
\newcommand{\R}{{\mathbb R}}
\newcommand{\dir}{\vec{d}}
\newcommand{\pol}{{\cal P}}
\newcommand{\robot}{{\cal R}}
\newcommand{\triang}{{\cal T}}
\newcommand{\dist}{\mathrm{dist}}
\newcommand{\eps}{\varepsilon}
\newcommand{\Mid}{:}
\newcommand{\ch}{\mathcal{CH}}
\newcommand{\exercise}[1]{\noindent{\bf Exercise #1:\\}}
\newcommand{\challenge}{\noindent{\bf Challenge problem:}}
\newenvironment{solution}{\begingroup\Solenv}{\END\endgroup}
\def\Solenv{\vspace{.5\baselineskip}\penalty100\advance\leftskip by\parindent
  \advance\leftmargini by\parindent
  \Sol\ \ignorespaces}
\def\Sol{\noindent{\bf Solution\/:}\nobreak}
\def\END{\unskip~\medbreak\medbreak\medbreak}
\def\qed{\hfill$\Box$}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}

\fancypagestyle{plain}{%
\fancyhf{}
\fancyhead[LO,RE]{\sffamily technische universiteit eindhoven}
\fancyhead[RO,LE]{\sffamily 2IMV20 Visualization}
\fancyfoot[LO,RE]{\sffamily /department of computer science}
\fancyfoot[RO,LE]{\sffamily\bfseries\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[RO,LE]{\sffamily 2IMV20 Visualization}
\fancyhead[LO,RE]{\sffamily technische universiteit eindhoven}
\fancyfoot[LO,RE]{\sffamily /department of computer science}
\fancyfoot[RO,LE]{\sffamily\bfseries\thepage}
\renewcommand{\headrulewidth}{1pt}
\renewcommand{\footrulewidth}{0pt}

%-------------------------------- Title ----------------------------------

\title{
Assignment 2
\vspace{5mm}
\hrule\vspace{1mm}
\textbf{\Huge Information visualisation and interaction}\\
\vspace{1mm}
\hrule
\vspace{5mm}
2IMV20 - Visualization\\
\today\\
\vspace{2cm}
Roy van den Hurk - 0817761\\
Perry van Wesel - 0818131\\
}
%\author{YOUR NAME}
\date{}

%--------------------------------- Text ----------------------------------

\begin{document}
\maketitle
\newpage

\tableofcontents
\newpage

\section{Introduction}
This document is aimed at designing a visualisation for a dataset on symptoms and diagnoses of several kinds of urinary system inflammation. Several visualisations are examined and implemented, then evaluated afterwards. The visualisations realised are made in html and javascript using \emph{d3.js}\cite{d3js}.

\section{Data overview}
The dataset considered in this report is the dataset on Urinary System Inflammation. This is a multivariate dataset with mostly binary variables, consisting of combinations of symptoms and diagnoses of urinary system inflammation. Various symptoms
have been recorded for each individual as well. This dataset originates from the \emph{UCI Machine Learning Repository}\cite{mlr} and was donated by H. Zarzcycki and J. Czerniak \cite{usi}.

\subsection{Interests}
The main goal of this dataset is to determine which symptoms can reliably be used to diagnose a patient with inflammation of the bladder, Nephritis of the pelvis origin or both. Determining such a set of symptoms can be useful as it gives the possibility to diagnose a patient without having to perform extra (paid) tests. This is obviously only possible if systems can give a reliable diagnosis.\\
\\
This means that for this particular dataset the combination of symptoms and diagnosis is important. More importantly, it would be nice to have a visualisation showing how accurately a certain symptom or combination of symptoms can contribute to a diagnosis. There are two variables that are of importance. Firstly there is coverage, which says something about how many of the cases with the diseases can be found with a set of symptoms. Secondly there is precision, which measures how many of the samples found with a certain combination is symptoms, are actually diagnosed. The goal would be to find a certain combination of variables that has high precision and high coverage.
\subsection{Tasks}
To easily draw useful conclusions from the dataset, the analyst should be able to get a general overview of all data entries, and should be able to sort these entries on several axes. Furthermore the data analyst should be able to highlight data points to get more information. As stated in the previous section, the ultimate goal is to compare combinations of symptoms. This means that one of the tasks should be to be able to find coverage and precision statistics on several statistics. It should be able to also easily spot connections between symptoms, to aid in finding good combinations.\\
An extension of these tasks would be to try to predict a certain diagnosis before it manifests, based on early symptoms. This would require extra data where several people are monitored over a period of time. This data would be difficult to obtain as it would require already predicting a diagnosis, or starting really early monitoring based on a few symptoms.

\section{Visualization}
The data has been visualized in four different ways that are each discussed in their own section.
\subsection{Overview heat map}
\begin{figure}[h]
\center
\includegraphics[scale=0.5]{overview}
\caption{Overview visualisation}
\label{fig:overview}
\end{figure}
To give an overview of all the data in the dataset, the visualisation contains a big heat map that shows every data entry as a line of cells. This is shown in figure \ref{fig:overview}. Hovering over a row displays a tooltip with a detailed description of the data entry. This overview is accompanied by a menu that allows for changing the ordering on the of the data. The ordering consists of all the attributes, and the order of importance can be changed, as well as giving the possibility to sort every individual attribute ascending or descending.\\
This visualisation gives a clear overview of all the data, deriving any connections is not very convenient in this form, which is something hopefully solved by the addition of the other visualisations. Another downside of this visualisation is that it takes up a lot of vertical space, meaning some strolling might be required depending on the amount of data.

\subsection{Symptom combination statistics heat map}
\begin{figure}[h]
\center
\includegraphics[width=\textwidth]{combinations}
\caption{Symptom combination statistics heat map with brushing and tooltip.}
\label{fig:comb}
\end{figure}
As the analysts analysing this data will ultimately want to find a suitable combination of symptoms to use when diagnosing patient, it is useful if they can somehow find the predictive power of each of these combinations of symptoms. To visualise these combinations a heat map was used, shown in figure \ref{fig:comb}. The visualised data has no order other than being partitioned into fields based on the amount of symptoms the combinations are build up of. Say there are some symptoms $A$ and $B$, then this visualisation will show data for $A, \neg A, B$ and $\neg B$ in the section of size one, and $A \cap B$, $\neg A \cap B$, $A \cap \neg B$ and $\neg A \cap \neg B$ in the section for size 2. The negation of each symptom is also added to this grid, as it could also contain useful information.\\
At the top of the heat map there is a small menu that lets the analyst determine firstly for which diagnosis to show statistics, and secondly on which property to color the cells of the heat map. The properties possible are coverage, precision and coverage times precision. Coverage represents the percentage of people found with the diagnosis out of all people, looking at this current set of symptoms. Precision represents the percentage of people found with this combination of symptoms that actually have the diagnosis.\\
As an example, consider a combination of symptoms that finds 5 out of 10 people with the diagnosis, this gives a coverage of 50\%. If the total number of people with this combination of symptoms is 6, and only 5 of those are diagnosed, that means the precision is $\frac{5}{6} = 84\%$. Obviously the ideal situation would be to have both coverage and precision high, therefore the third option is the product of these two statistics.\\
This grid uses brushing: hovering over a cell in the grid will highlight any cells that contain the same values as the one hovered over. All data entries that have this combination of symptoms are also highlighted in the overview heat map.\\
The downside of this visualisation is that it works best with binary variables, so the temperature variable is not included.

\subsection{Parallel sets}
\begin{figure}[h]
\center
\includegraphics[width=\textwidth]{parsets}
\caption{Parallel set visualisation of the data set with discretized temperature ranges.}
\label{fig:parsets}
\end{figure}
The parallel set visualisation \cite{parsets} , as shown in figure \ref{fig:parsets} provides a way to explore the dataset and find connections between different variables. Every level shows the different possible values for one variable, and divides up all streams coming from higher levels. This means that every stream represents a different combination of variables, similar to the combinations heat map. Each variable can be moved up or down in the order, allowing for better comparisons between two specific variables. The values of each variable can also be rearranged, allowing the analyst to manage the different streams. Hovering over a stream will highlight every child streams created at lower levels. It will also highlight the path up the levels to show where it came from. A tooltip is shown to give exact data.\\
This visualisation is capable of also showing temperature, although the continuous range still needs to be discretized. A downside of this visualisation is that it does not really give clear info on either coverage or precision of different symptom pairs. It is however simple to take the symptom combination from the tooltip and simply look it up in the combinations statistics heat map.

\subsection{Bar chart}


\begin{figure}[h]
\center
\includegraphics[width=\textwidth]{barchart}
\caption{Bar chart visualisation of the data set with discretized temperature ranges.}
\label{fig:barchart}
\end{figure}

The bar chart visualisation \ref{fig:barchart} allows the data analyst to visualize a combination of one or more symptoms. For every symptom it can be indicated if the visualisation should show data having symptom (green), not having the symptom (red) or either one of those (white). The temperature is split into several intervals and placed along the x-axis. Along the y-axis the percentage of samples matching the selected symptoms and being diagnosed with either nephritis(blue) or inflammation(green) is shown. Furthermore, the number of patients diagnosed with both is shown in red as there is a difference in 100\% of the patients having either inflammation or nephritis or 50\% having both of the previously mentioned diseases. The total number of samples matching the selected symptoms is visualised in the form of a purple bar as it would be impossible to see whether there are no matching samples or that there are no samples with a diagnosed inflammation or nephritis without said bar. Moreover, when hovering over any of the bars the precise number of matching samples is shown. The main purpose of the bar chart is to give an overview of what percentage of samples with the given symptoms lead to a diagnosis of nephritis or inflammation.\\

Even though the bar chart would be capable of showing the entire range of temperatures, the decision was made to show a discretized version as less bars are easier to comprehend. A negative aspect of the bar chart is that it, just as the parallel sets, harder to view precision of the selected symptoms. A pro of the bar chart is that is easily shows the coverage across all temperature brackets as well as allows for an easy selection of all symptom combinations.

\subsection{Force-directed graph}

\begin{figure}[H]
\center
\includegraphics[width=\textwidth]{force}
\caption{Force-directed graph visualisation}
\label{fig:force}
\end{figure}

Using a force-directed graph \ref{fig:force} was also tried to visualise the data. This graph displays every symptom as a node connected to its diagnosis via an edge. However, as seen in the image very little extra information compared to the other visualisation methods can be extracted. Further improvements have been tried in which every node with the same attribute are interconnected to show groups that have many links to a certain diagnosis. However, the performance was to bad to be a useful visualisation. Combinations of symptoms would be impossible to run. So this visualisation method was not included in the final application.


\section{Results}
By analyzing the several visualizations one can see that temperature is one of the biggest indications of nephritis. Out of the 60 people with a temperature of 38 or higher 50 (83\%) were diagnosed with nephritis. Furthermore, no one in the data set with a temperature below 38 was diagnosed with nephritis. This can easily be determined by taking a quick glance at either the parallel sets or the bar chart visualization. User interaction with the parallel sets clearly highlight a path for every temperature bracket to a diagnosis and the bar chart shows bars of near a 100\% in both the 38-40 and $>$ 40 temperature brackets. \\
In a similar fashion one can see that temperature is not a clear indicator of inflammation as inflammation occurs more often at a normal body temperature than at an elevated body temperature. \\

The next symptom is urine pushing, which is a very clear indication of inflammation, the data shows that everyone that has inflammation also has urine pushing as a symptom. However, the other way around this drops to ~75\%, which is still significant. \\
For the diagnosis of of nephritis on the other hand, a large percentage of people with nephritis are bothered by urine pushing, however of the people that have problems with urine pushing only around half actually have nephritis. \\
Thus we can conclude that not having urine pushing problems means that the patient in question has a low chance of having nephritis as well as a low chance of having inflammation.  \\

One of the other symptoms in the data set is lumber pain. The bar chart clearly shows that having lumber pain leads to a diagnosis of nephritis for all samples. Furthermore, the parallel set representation of the data shows that there is a strong correlation between temperature and having lumber pain, out of the 60 samples with a temperature over 38 50 samples show signs of lumber pain. \\
On the other hand, having lumber pain or not does not seem to be related to inflammation at all as around half of the people with lumber pain have no inflammation and also around half of the people without lumber pain do have inflammation.\\

In the case of inflammation, nausea also seems to be insufficient in diagnosing a patient. For diagnosing nephritis nausea is a good symptom as a 100 \% of the people with nausea are diagnosed with nephritis. However, not having nausea does not mean that a patient does not have nephritis.\\

Micturition pains is another symptom that is unreliable when making a diagnosis as whether a patient has micturition pain does not relate to having inflammation or nephritis at all. 

The final symptom in the data set is a burning feeling, itchy feeling or swelling of the urethra outlet. This symptom alone is not viable to make a diagnosis of either nephritis or inflammation, however combined with a elevated temperature burning, itching or swelling is present in all of the patients diagnosed with nephritis. \\

Looking at only individual symptoms can give a reasonable preemptive diagnosis, however more often than not more symptoms make for a better diagnosis. Take for example the combination of nausea and urine pushing, which leads to a diagnosis of inflammation in 100\% of the sample patients, however around 70\% of the patients without symptoms have inflammation as well. Looking at combinations of  more than three symptoms further strengthens the fact this. Take fore example the following triples: symptoms nausea, urine pushing and micturition pains, lumber pain, urine pushing and burning/itching/swelling, nausea, lumber pain, micturition pains. These all lead to a diagnosis of nephritis. A lot of other combinations lead to a definite diagnosis of nephritis as well. \\

At last we can conclude that the most important symptoms in diagnosing inflammation are urine pushing or a combination of burning/itching/swelling and either nausea or micturition pains.\\
In the case of nephritis an elevated temperature as well as nausea or lumber pain are good indication of said disease.\\

The way that the application was designed made it easy to select the symptoms a patient has and in return get various data points about the various diagnoses, either inflammation, nephritis, both or neither. These data points include how many samples there are with the given symptoms as well as what percentage of these samples were diagnosed with any of the diseases. Furthermore, the combination of a heat map, a bar chart and a parallel set made it easy to determine a possible preemptive diagnosis based on give symptoms as well as determine a set of symptoms that are likely to be symptoms of inflammation or nephritis. Interaction with the various visualisation methods was also a key element of the application as with a data set of around 120 entries with 8 attributes each it is impossible to show every entry while still being able to get a global overview. Without interaction every combination had to be shown at the same time. Being able to filter based on a set of symptoms made the analysis of the data a lot easier. Furthermore, the heat map shows potential groups of symptoms easily and interaction allows the data analyst to get more information about said group.  \\

During the data exploration process it was noticed that the bar chart lacked some much needed functionality. That is, the total number of samples matching the selected symptoms for each temperature bracket. Without this total number it was impossible to see whether an absence of a bar meant that there were no matching samples or that none of the matching samples were diagnosed with a disease. The total number was added in the form of an extra bar. \\

Furthermore, although the heat map allows one to quickly identify a set of symptoms of inflammation or nephritis, or on the opposite a set of symptoms that are not symptoms of the diseases. It is hard find a cell if you have a set of symptoms as there is no clear labeling of the cell until the user highlights said cell. However, combined with the bar chart and the parallel set representation, which in addition show temperature information, a full overview is provided. \\

There were also made minor changes to the parallel set as the original tool tip showed for every path the values from the previous symptoms as a sequence of yes's and no's. However, this made it hard to see which attributes have which values. The updated version uses a tool tip which explicitly shows the values as well as the corresponding attributes. \\

Another thing that the visualisation application lacks is support for combinations of symptoms of which only a certain number of symptoms have to be present. That is, take symptom A and B than the application will show information of samples for which A $\cup$ B holds. The decision was made to not incorporate this in the final version of the application as the data was still available albeit in a slightly more cumbersome way by manually trying every combination.

\section{Division of tasks}
Perry implemented the overview heat map, the symptom combination statistics heat map and the parallel sets representation. \\
Roy was responsible for analyzing the data as well as implementing the bar chart and tried out the force-directed graph. \\

\bibliography{bib}
\bibliographystyle{plain}
\end{document}