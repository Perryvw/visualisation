
d3.csv('inflammation.csv', function (data) {
    data.forEach(function (d) {
        for (var k in d) {
            if (d[k] == "yes") {
                d[k] = true;
            }
            if (d[k] == "no") {
                d[k] = false;
            }
        }
    });
    var attributes = [
        {
            "name": "nausea",
            "inflammation_count": 0,
            "nephritis_count": 0,
            "index": 0,
            "neg_index": 5,
            "negated": false
        },
        {
            "name": "lumber pain",
            "inflammation_count": 0,
            "nephritis_count": 0,
            "index": 1,
            "neg_index": 6,
            "negated": false
        },
        {
            "name": "urine pushing",
            "inflammation_count": 0,
            "index": 2,
            "neg_index": 7,
            "negated": false
        },
        {
            "name": "micturition pains",
            "inflammation_count": 0,
            "nephritis_count": 0,
            "index": 3,
            "neg_index": 8,
            "negated": false
        },
        {
            "name": "burning/itch/swelling",
            "inflammation_count": 0,
            "nephritis_count": 0,
            "index": 4,
            "neg_index": 9,
            "negated": true
        },
        {
            "name": "not nausea",
            "inflammation_count": 0,
            "nephritis_count": 0,
            "index": 5,
            "neg_index": 1,
            "negated": true
        },
        {
            "name": "not lumber pain",
            "inflammation_count": 0,
            "nephritis_count": 0,
            "index": 6,
            "neg_index": 2,
            "negated": true
        },
        {
            "name": "not urine pushing",
            "inflammation_count": 0,
            "nephritis_count": 0,
            "index": 7,
            "neg_index": 3,
            "negated": true
        },
        {
            "name": "not micturition pains",
            "inflammation_count": 0,
            "nephritis_count": 0,
            "index": 8,
            "neg_index": 4,
            "negated": true
        },
        {
            "name": "not burning/itch/swelling",
            "inflammation_count": 0,
            "nephritis_count": 0,
            "index": 9,
            "neg_index": 5,
            "negated": true
        }
    ];

    var attrToIndex = {
        "nausea": 0,
        "lumber pain": 1,
        "urine pushing": 2,
        "micturition pains": 3,
        "burning/itch/swelling": 4,
        "not nausea": 5,
        "not lumber pain": 6,
        "not urine pushing": 7,
        "not micturition pains": 8,
        "not burning/itch/swelling": 9
    };

    var margin = {top: 120, right: 100, bottom: 10, left: 175},
        width = 500,
        height = 500;

    var x = d3.scale.ordinal().rangeBands([0, width]),
        z = d3.scale.linear().domain([0, data.length]).range([0.25, 1]),
        c = d3.scale.category10().domain(d3.range(10)),
        s = d3.scale.ordinal().range([0, 100]);
    var color = d3.scale.linear()
        .domain([0, 0.5, 1])
        .range(["#d8b365", "#f5f5f5", "#5ab4ac"]);

    var svg = d3.select("body").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .style("margin-left", margin.left + "px")
        .style("margin-top", margin.top + "px")
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var matrix = [],
        nodes = attributes,
        n = attributes.length;


// Precompute the orders.
    var orders = {
        name: d3.range(n).sort(function (a, b) {
            return d3.ascending(nodes[a].name, nodes[b].name);
        }),
        name_2: d3.range(n).sort(function (a, b) {
            return d3.ascending(nodes[a].name.replace('not', 'zzz'), nodes[b].name.replace('not', 'zzz'));
        }),
        inf: d3.range(n).sort(function (a, b) {
            return nodes[b].inflammation_count - nodes[a].inflammation_count;
        }),
        nep: d3.range(n).sort(function (a, b) {
            return nodes[b].nephritis_count - nodes[a].nephritis_count;
        })
    };

    setData();

    d3.select("#nausea").on("input", function() {
        setData();
    });

// The default sort order.
    x.domain(orders.name);

    svg.append("rect")
        .attr("class", "background")
        .attr("width", width)
        .attr("height", height);

    var row = svg.selectAll(".row")
        .data(matrix)
        .enter().append("g")
        .attr("class", "row")
        .attr("transform", function (d, i) {
            return "translate(0," + x(i) + ")";
        })
        .each(row);

    row.append("line")
        .attr("x2", width);

    row.append("text")
        .attr("x", -6)
        .attr("y", x.rangeBand() / 5)
        .attr("dy", ".32em")
        .attr("text-anchor", "end")
        .text(function (d, i) {
            return nodes[i].name;
        });

    var column = svg.selectAll(".column")
        .data(matrix)
        .enter().append("g")
        .attr("class", "column")
        .attr("transform", function (d, i) {
            return "translate(" + x(i) + ")rotate(-90)";
        });

    column.append("line")
        .attr("x1", -width);

    column.append("text")
        .attr("x", 6)
        .attr("y", x.rangeBand() / 5)
        .attr("dy", ".32em")
        .attr("text-anchor", "start")
        .text(function (d, i) {
            return nodes[i].name;
        });

    function row(row) {
        var cell = d3.select(this).selectAll(".cell")
            .data(row)
            .enter().append("rect")
            .attr("class", "cell")
            .attr("x", function (d) {
                return x(d.x);
            })
            .attr("width", x.rangeBand())
            .attr("height", x.rangeBand())
            .style("fill-opacity", function (d) {
                return 1;
            })
            .style("fill", function (d) {
                var percentage = d.inflammation_total_count > 0 ? d.inflammation_count / d.inflammation_total_count : 0;
                return color(percentage);
            })
            .on("mouseover", mouseover)
            .on("mouseout", mouseout);
    }

    function mouseover(p) {
        d3.selectAll(".row text").classed("active", function (d, i) {
            return i == p.y;
        });
        d3.selectAll(".column text").classed("active", function (d, i) {
            return i == p.x;
        });
    }

    function mouseout() {
        d3.selectAll("text").classed("active", false);
    }

    d3.select("#order").on("change", function () {
        clearTimeout(timeout);
        order(this.value);
    });

    function order(value) {
        x.domain(orders[value]);

        var t = svg.transition().duration(2500);

        t.selectAll(".row")
            .delay(function (d, i) {
                return x(i) * 4;
            })
            .attr("transform", function (d, i) {
                return "translate(0," + x(i) + ")";
            })
            .selectAll(".cell")
            .delay(function (d) {
                return x(d.x) * 4;
            })
            .attr("x", function (d) {
                return x(d.x);
            });

        t.selectAll(".column")
            .delay(function (d, i) {
                return x(i) * 4;
            })
            .attr("transform", function (d, i) {
                return "translate(" + x(i) + ")rotate(-90)";
            });
    }

    var timeout = setTimeout(function () {
        order("name");
      //  d3.select("#order").property("selectedIndex", 2).node().focus();
    }, 5000);

    function setData(){
        for (var i = 0; i < n; i++) {
            matrix[i] = d3.range(n).map(function (j) {
                return {
                    x: j, y: i,
                    inflammation_count: 0, nephritis_count: 0,
                    inflammation_total_count: 0, nephritis_total_count: 0
                };
            });
        }

        for(var i =0;i<n;i++){
            for(var j =0;j<n;j++){
                matrix[i][j].inflammation_count = 0;
                matrix[i][j].nephritis_count = 0;
                matrix[i][j].inflammation_total_count = 0;
                matrix[i][j].nephritis_total_count = 0;
            }
        };

        data.forEach(function (entry) {
            for (var key1 in entry) {
                for (var key2 in entry) {
                    if (!(entry.hasOwnProperty(key1) || entry.hasOwnProperty(key2))) {
                        continue;
                    }
                    var exclude = ["temperature", "inflammation", "nephritis"];
                    if (exclude.includes(key1) || exclude.includes(key2)) {
                        continue;
                    }
                    var attr1 = attributes[attrToIndex[key1]];
                    var attr2 = attributes[attrToIndex[key2]];

                    matrix[entry[key1] ? attr1.index : attr1.neg_index][entry[key2] ? attr2.index : attr2.neg_index].inflammation_count += (entry["inflammation"] ? 1 : 0);
                    matrix[entry[key1] ? attr1.index : attr1.neg_index][entry[key2] ? attr2.index : attr2.neg_index].nephritis_count += (entry["nephritis"] ? 1 : 0);

                    matrix[entry[key1] ? attr1.index : attr1.neg_index][entry[key2] ? attr2.index : attr2.neg_index].inflammation_total_count += 1;
                    matrix[entry[key1] ? attr1.index : attr1.neg_index][entry[key2] ? attr2.index : attr2.neg_index].nephritis_total_count += 1;

                    //everything counted twice
                    attributes[entry[key1] ? attr1.index : attr1.neg_index].inflammation_count += (entry["inflammation"] ? 0.5 : 0);
                    attributes[entry[key1] ? attr1.index : attr1.neg_index].nephritis_count += (entry["nephritis"] ? 0.5 : 0);

                    attributes[entry[key2] ? attr2.index : attr2.neg_index].inflammation_count += (entry["inflammation"] ? 0.5 : 0);
                    attributes[entry[key2] ? attr2.index : attr2.neg_index].nephritis_count += (entry["nephritis"] ? 0.5 : 0);
                }
            }
        });

        svg.selectAll(".row")
            .data(matrix)
            .filter(function(a){return false});

        svg.selectAll(".column")
            .data(matrix);
    }
});