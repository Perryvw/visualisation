var attributes = [
	{k:0,a:true,v:"temperature"},
	{k:1,a:true,v:"nausea"},
	{k:2,a:true,v:"lumber pain"},
	{k:3,a:true,v:"urine pushing"},
	{k:4,a:true,v:"micturition pains"},
	{k:5,a:true,v:"burning/itch/swelling"},
	{k:6,a:true,v:"inflammation"},
	{k:7,a:true,v:"nephritis"}
	];

var cellSize = 20;

var color = d3.scale.linear()
        .domain([0, 0.5, 1])
        .range(["#f7fcb9", "#addd8e", "#31a354"]);

// load csv file and create the chart
d3.csv('inflammation.csv', function(data) {
	data.forEach(function(d) {
		for( var k in d ) {
			if (d[k] == "yes") {
				d[k] = true;
			}
			if (d[k] == "no") {
				d[k] = false;
			}
		}
	});

	var tooltip = d3.select("body").append("div")
        .attr("class", "tooltip3");
	
	var topOffset = 160;

	var svg = d3.select("#graph").append("svg")
	.attr("width", cellSize * 8)
	.attr("height", data.length * cellSize/3 + topOffset + 100);

	//Legend gradient
    svg.append("linearGradient")
      .attr("id", "olgrad")
      .attr("x1", "0%").attr("y1", "0%")
      .attr("x2", "100%").attr("y2", "0%")
    .selectAll("stop")
      .data([
        {offset: "0%", color: "#f7fcb9"},
        {offset: "50%", color: "#addd8e"},
        {offset: "100%", color: "#31a354"}
      ])
    .enter().append("stop")
      .attr("offset", function(d) { return d.offset; })
      .attr("stop-color", function(d) { return d.color; });

	 //Legend
	var legendOffset = Math.round(data.length * cellSize/3) + topOffset;
    svg.append("text").text("Legend:").attr("x", 10).attr("y", legendOffset + 20);
    svg.append("text").text("Yes").attr("x", 50).attr("y", legendOffset + 40);
    svg.append("text").text("No").attr("x", 50).attr("y", legendOffset + 60);
    svg.append("text").text("35.5 C°").attr("x", 20).attr("y", legendOffset + 100);
    svg.append("text").text("41.5 C°").attr("x", 90).attr("y", legendOffset + 100);
    svg.append("rect").text("Legend:").attr("x", 10).attr("y", legendOffset + 30)
    	.attr("width",cellSize).attr("height", cellSize/2).attr("stroke", "#000").attr("fill", "#FF7F0E");
    svg.append("rect").text("Legend:").attr("x", 10).attr("y", legendOffset + 50)
    	.attr("width",cellSize).attr("height", cellSize/2).attr("stroke", "#000").attr("fill", "#2D2D2D");
    svg.append("rect").text("Legend:").attr("x", 30).attr("y", legendOffset + 70)
    	.attr("width",90).attr("height", cellSize/2).attr("stroke", "#000").attr("fill", "url(#olgrad)");
    //svg.append("text").text("Low").attr("x", 510).attr("y", sectHeight * cellSize + 70);
    //svg.append("text").text("High").attr("x", 570).attr("y", sectHeight * cellSize + 70);
    //svg.append("rect").text("High").attr("x", 510).attr("y", sectHeight * cellSize + 35).attr("width", 90).attr("height",20)
        //.attr("fill","url(#legendGrad)").attr("stroke", "#000");

	svg.selectAll(".columLabel").data(attributes)
		.enter()
		.append("text")
		.text(function(d) { return d.v; })
		.attr("transform", "rotate(270)")
		.attr("x", -150)
		.attr("y", function(d,i) { return i * 20 +15; });

	var entry = svg.selectAll(".datapoint").data(data).enter().
		append("g")
		.attr("transform", function(d,i) { return "translate(" + 0 + "," + (i * cellSize/3 + topOffset) + ")"; })
		.attr("width", cellSize * 8 )
		.attr("height", cellSize/3)
		.attr("x", 0)
		.attr("y", function(d,i) { return cellSize * i/3 + topOffset; })
		.attr("class", function(d) {
            var c = ["datapoint"];
            for (var k in d) {
                if (k != "d1" && k != "d2" && k != "d3") {
                    if (d[k]){
                        c.push(k.replaceAll("[ /]","-"));
                    } else {
                        c.push("-"+k.replaceAll("[ /]","-"));
                    }
                }   
            }
            return c.join(" ");
        })
        .on("mouseover", function(d) {
            var pos = d3.event.target.getBoundingClientRect();
            tooltip.attr("style", "left: "+(170)+"; top: "+(pos.top + window.pageYOffset)+"; opacity: 1;");
            d3.select(d3.event.target.parentNode).selectAll("rect")
            	.attr("stroke","#fff");
        })
        .on("mouseout", function(d) {
            tooltip.attr("style", "opacity: 0;")
            .html(function() {
            	var str = "";
            	for (var k in d) {
            		str += "<b>"+k.charAt(0).toUpperCase() + k.slice(1) +"</b>: "+d[k]+"<br>";
            	}
            	return str;
            });
            d3.select(d3.event.target.parentNode).selectAll("rect")
            	.attr("stroke","#000");
        });;

	attributes.forEach(function( attr ) {
		entry.append("rect")
		.attr("stroke", "#000")
		.attr("width", cellSize)
		.attr("height", cellSize/3)
		.attr("x", cellSize * attr.k )
		.attr("y", 0)
		.attr("class", function(d) {
			if (d[attr.v] === true || d[attr.v] === false) {
				return d[attr.v] ? "boolTrue" : "boolFalse";
			} else {
				return "number";
			}
		});
	});

	entry.selectAll(".number").attr("fill", function(d) { return color(interpVal(35, 42, d.temperature).toString()); });

	updateOrder();

	var orderBars = d3.select("#graphOrder").selectAll(".orderBar").data( attributes, function(d) { return d.k; } )
		.enter().append("div")
		.attr("class", "orderBar")
		.text(function(d) { return d.v; });

	orderBars.append("div")
		.attr("class","triangleUpBig")
		.on("click", ascArrowClick);

	orderBars.append("div")
		.attr("class","triangleDown")
		.on("click", orderElemDown);

	orderBars.append("div")
		.attr("class","triangleUp")
		.on("click", orderElemUp);

	function descArrowClick( d ) {
		d3.select(d3.event.target)
			.classed("triangleUpBig", true)
			.classed("triangleDownBig", false)
			.on("click", ascArrowClick);
		d.a = true;

		updateOrder();
	}

	function ascArrowClick( d ) {
		d3.select(d3.event.target)
			.classed("triangleUpBig", false)
			.classed("triangleDownBig", true)
			.on("click", descArrowClick);
		d.a = false;

		updateOrder();
	}

	function orderElemUp( d, i ) {
		if ( d.k > 0 ) {
			var s = d3.select("#graphOrder").selectAll(".orderBar");

			var data = s.data();
			data[d.k-1].k = d.k;
			d.k--;

			s.sort(function(a,b) { return a.k - b.k; }).order();
		}

		updateOrder();
	}

	function orderElemDown( d, i ) {
		if ( d.k < attributes.length - 1 ) {
			var s = d3.select("#graphOrder").selectAll(".orderBar");

			var data = s.data();
			data[d.k+1].k = d.k;
			d.k++;

			s.sort(function(a,b) { return a.k - b.k; });

			s.order();
		}

		updateOrder();
	}

	function updateOrder() {
		var order = attributes.sort(function(a,b){return a.k-b.k;});
		var cmp = d3.comparator();
		order.forEach(function(o) {
			cmp.order(o.a ? d3.ascending : d3.descending, function(d) { return d[o.v]; })
		});

		svg.selectAll(".datapoint").sort(cmp)
		.transition()
			.duration(750)
			.attr("transform", function(d,i) { return "translate(" + 0 + "," + (i * cellSize/3 + topOffset) + ")"; });
	}

	function interpVal( low, high, val ) {
		return (val - low)/(high-low);
	}

	String.prototype.replaceAll = function(search, replacement) {
	    var target = this;
	    return target.replace(new RegExp(search, 'g'), replacement);
	};
});

