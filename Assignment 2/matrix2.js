//temperature,"nausea","lumber pain","urine pushing","micturition pains","burning/itch/swelling","inflammation","nephritis"
/*var data = [
 /!*  {
 "temperature": 35.5,
 "nausea": true,
 "lumber pain": true,
 "urine pushing": false,
 "micturition pains": false,
 "burning/itch/swelling": false,
 "inflammation": true,
 "nephritis": false
 },*!/
 {
 "temperature": 35.5,
 "nausea": false,
 "lumber pain": false,
 "urine pushing": false,
 "micturition pains": true,
 "burning/itch/swelling": true,
 "inflammation": true,
 "nephritis": true
 }
 ];*/

d3.csv('inflammation.csv', function (data) {
    data.forEach(function(d) {
        for( var k in d ) {
            if (d[k] == "yes") {
                d[k] = true;
            }
            if (d[k] == "no") {
                d[k] = false;
            }
        }
    });

    var rowLabels = ["inflammation", "nephritis"];
    var columnLabels = ["nausea", "lumber pain", "urine pushing", "micturition pains", "burning/itch/swelling"];
    var perm = 5;

    var chart = d3.select("#matrix2").append("svg")
        .attr("width", 700)
        .attr("height", 300);

    var vers = [];
    var combs = combinations(columnLabels);
    combs.forEach(function(d){
        var v = vers[d.length];
        if (!v) {
            vers[d.length] = [];
            v = vers[d.length];
        }
        vers[d.length] = vers[d.length].concat(calcVers(d));
    });

    for (var c in vers) {
        vers[c].forEach(function(v) {
            var t = 0;
            var t2 = 0;
            var t3 = 0;
            data.forEach(function(d) {
                var same = true;
                for (var k in v) {
                    if (d[k] != v[k]) {
                        same = false;
                        break;
                    }
                }

                if (same) {
                    t++;
                    if (d.inflammation) {
                        t2++;
                    }
                    if (d.nephritis) {
                        t3++;
                    }
                }
                if (same && v.nausea && v['lumber pain'] && v['urine pushing'] && v['micturition pains'] && v['burning/itch/swelling']) {
                    //console.log(v,d);
                }
            });
            v.d1 = t;
            v.d2 = t2;
            v.d3 = t3;
        });
    }

    var cellSize = 20;
    var sectHeight = 10;
    var offsets = [100];
    for (var i=1;i<=columnLabels.length;i++) {
        offsets[i] = offsets[i-1] + Math.ceil(vers[i].length/sectHeight)*cellSize + 10;
    }

    var totalInflammation = 0;
    var totalNephritis = 0;
    data.forEach(function(d) {
        if (d.inflammation) totalInflammation++;
        if (d.nephritis) totalNephritis++;
    });

    var color = d3.scale.linear()
        .domain([0, 1])
        .range(["#edf8b1", "#2c7fb8"]);

    var tooltip = d3.select("body").append("div")
        .attr("class", "tooltip2");

    var sections = chart.selectAll(".section").data(vers)
        .enter()
        .append("g").filter(function(d,i) { return i > 0; })
        .attr("transform", function(d,i) { return "translate(" + offsets[i] + "," + 25 + ")"; })
        .attr("width", function(d) { return Math.ceil(d.length/sectHeight)*cellSize; })
        .attr("height", sectHeight * cellSize )
        .attr("class","section");

    chart.append("text").text("Combinations").attr("color","#000").attr("y", sectHeight * cellSize/2 + 25);
    chart.append("text").text("Comb. size: ").attr("color","#000").attr("y", 20).attr("x", 15);

    //Size
    sections.append("text")
        .text(function(d,i){ return i+1; })
        .attr("y", -5)
        .attr("x", function(d) { return Math.ceil(d.length/sectHeight)*(cellSize/2) - 5; });

    //Legend gradient
    chart.append("linearGradient")
      .attr("id", "legendGrad")
      .attr("x1", "0%").attr("y1", "0%")
      .attr("x2", "100%").attr("y2", "0%")
    .selectAll("stop")
      .data([
        {offset: "0%", color: "#edf8b1"},
        {offset: "100%", color: "#2c7fb8"}
      ])
    .enter().append("stop")
      .attr("offset", function(d) { return d.offset; })
      .attr("stop-color", function(d) { return d.color; });

    //Legend
    chart.append("text").text("Legend:").attr("x", 450).attr("y", sectHeight * cellSize + 50);
    chart.append("text").text("Low").attr("x", 510).attr("y", sectHeight * cellSize + 70);
    chart.append("text").text("High").attr("x", 570).attr("y", sectHeight * cellSize + 70);
    chart.append("rect").text("High").attr("x", 510).attr("y", sectHeight * cellSize + 35).attr("width", 90).attr("height",20)
        .attr("fill","url(#legendGrad)").attr("stroke", "#000");


    var blocks = sections.selectAll(".block").data(function(d){return d;})
        .enter()
        .append("rect")
        .attr("class", function(d) {
            var c = ["block"];
            for (var k in d) {
                if (k != "d1" && k != "d2" && k != "d3") {
                    if (d[k]){
                        c.push(k.replaceAll("[ /]","-"));
                    } else {
                        c.push("-"+k.replaceAll("[ /]","-"));
                    }
                }   
            }
            return c.join(" ");
        })
        .attr("x", function(d,i){ return Math.floor(i/sectHeight)*cellSize; })
        .attr("y", function(d,i){ return Math.floor(i%sectHeight)*cellSize; })
        .attr("width", cellSize)
        .attr("height", cellSize)
        .attr("stroke", "#000")
        .attr("fill", function(d) { 
            if (d.d1 > 0 ) return color(d.d2/totalInflammation);
            return "#222222";
        })
        .on("mouseover", function(d) {
            var pos = d3.event.target.getBoundingClientRect();
            tooltip.html(function(){ 
                var t = "";
                for (var k in d) {
                    if (k != "d1" && k != "d2" && k != "d3") {
                        t += (d[k] ? k : "<font color='#ff0000'>not "+k+'</font>')+"<br>";
                    }
                }
                t += "<br>";
                if (d.d1 > 0) {
                    if (dataset == 1){
                        t += "Hits: "+d.d1+"<br>"
                        t += "Coverage (good hits/total good): (<b>"+(Math.floor(1000* d.d3/totalNephritis)/10)+"%)</b><br>";
                        t += "Precision (good hit/ all hits): (<b>"+(Math.floor(1000* d.d3/d.d1)/10)+"%)</b>";
                    } else {
                        t += "Hits: "+d.d1+"<br>"
                        t += "Coverage (good hits/total good): (<b>"+(Math.floor(1000* d.d2/totalInflammation)/10)+"%)</b><br>";
                        t += "Precision (good hit/ all hits): (<b>"+(Math.floor(1000* d.d2/d.d1)/10)+"%)</b>";
                    }
                } else {
                    t += "<b>No matches.</b>";
                }
                return t;
            })
            .attr("style", "left: "+(pos.left - tooltip.node().getBoundingClientRect().width - 5)+"; top: "+(pos.top + window.pageYOffset)+"; opacity: 1;");

            var selector = "";
            for (var k in d) {
                if (k != "d1" && k != "d2" && k != "d3") {
                    selector = selector + (d[k] ? "."+k.replaceAll("[ /]","-") : ".-"+k.replaceAll("[ /]","-"));
                }
            }
            sections.selectAll(selector).attr("stroke", "#00ffff").attr("stroke-width", 4).attr("width",cellSize-2).attr("height",cellSize-2);
            d3.select("#graph").selectAll(selector+" rect").attr("stroke", "#fff");
        })
        .on("mouseout", function(d) {
            tooltip.attr("style", "opacity: 0;");
            sections.selectAll(".block").attr("stroke", "#000").attr("stroke-width", 1).attr("width",cellSize).attr("height",cellSize);
            d3.select("#graph").selectAll(".datapoint rect").attr("stroke", "#000");
        }); 

    var dataset = 0;
    var coloring = 0;
    d3.selectAll(".colorRadio").on("click", function(d) {
        if (d3.event.target.value == "co") {
           coloring = 0;
        } else if (d3.event.target.value == "pr") {
            coloring = 1;
        } else {
            coloring = 2;
        }

        updateColors();
    });

    d3.select("#matrix2Select").on("change", function() {
        if (d3.event.target.value == "i") {
            dataset = 0;
        } else if (d3.event.target.value == "n") {
            dataset = 1;
        } else {

        }

        updateColors();
    }); 

    function updateColors() {
        if (dataset == 0) {
            if (coloring == 0) {
                blocks.transition(2000)
                .attr("fill", function(d) { 
                    if (d.d1 > 0 ) return color(d.d2/totalInflammation);
                    return "#222222";
                });
            } else if (coloring == 1) {
                blocks.transition(2000)
                .attr("fill", function(d) { 
                    if (d.d1 > 0 ) return color(d.d2/d.d1);
                    return "#222222";
                });
            } else {
                blocks.transition(2000)
                .attr("fill", function(d) {
                    if (d.d1 > 0 ) return color((d.d2/totalInflammation)*(d.d2/d.d1));
                    return "#222222";
                });
            }
        } else if (dataset == 1) {
            if (coloring == 0) {
                blocks.transition(2000)
                .attr("fill", function(d) { 
                    if (d.d1 > 0 ) return color(d.d3/totalNephritis);
                    return "#222222";
                });
            } else if (coloring == 1) {
                blocks.transition(2000)
                .attr("fill", function(d) { 
                    if (d.d1 > 0 ) return color(d.d3/d.d1);
                    return "#222222";
                });
            } else {
                blocks.transition(2000)
                .attr("fill", function(d) {
                    if (d.d1 > 0 ) return color((d.d3/totalNephritis)*(d.d3/d.d1));
                    return "#222222";
                });
            }
        }
    }
});

function cross(a, b) {
    var c = [], n = a.length, m = b.length, i, j;
    for (i = -1; ++i < n;) for (j = -1; ++j < m;) c.push({x: a[i], i: i, y: b[j], j: j});
    return c;
}

function calcVers( inp ) {
    out = [];
    for (var i=0;i<Math.pow(2,inp.length);i++) {
        var obj = {};
        for (var j=0; j < inp.length; j++) {
            obj[inp[j]] = (i & (1 << j)) != 0;
        }
        out.push(obj);
    }
    return out;
}

function combinations(inp) {
    var fn = function(active, rest, a) {
        if (active.length == 0 && rest.length == 0)
            return;
        if (rest.length == 0) {
            a.push(active);
        } else {
            var n = active.slice();
            n.push(rest[0]);
            fn(n, rest.slice(1), a);
            fn(active.slice(), rest.slice(1), a);
        }
        return a;
    }
    return fn([], inp, []);
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};