(function() {
/**
 * Created by Roy van den Hurk on 29-1-2016.
 * Adapted from http://bl.ocks.org/mbostock/3887051
 */
var margin = {top: 20, right: 120, bottom: 60, left: 40},
    width = 750 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var x0 = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1);

var x1 = d3.scale.ordinal();

var y = d3.scale.linear()
    .range([height, 0]);

var color = d3.scale.ordinal()
    .range(["#e41a1c", "#377eb8", "#4daf4a", "#984ea3"]);

var xAxis = d3.svg.axis()
    .scale(x0)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .tickFormat(d3.format(".2s"));

var svg = d3.select("#barchart")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

makeTriState("nausea");
makeTriState("lumber_pain");
makeTriState("urine_pushing");
makeTriState("micturition_pains");
makeTriState("burning_itch_swelling");

function makeTriState(name) {
    document.getElementById(name).onclick = (function () {

        var i = 0;
        var states = ['-1', '1', '0'];

        return function () {
            i = ++i % states.length;
            this.value = states[i];
        };
    })();
}

d3.csv('inflammation.csv', function (error, csv) {
    if (error) {
        throw error
    }

    csv.forEach(function (d) {
        if (parseInt(d["temperature"]) < 38) d["temperature"] = "< 38";
        if (parseInt(d["temperature"]) >= 38 && parseInt(d["temperature"]) < 40) d["temperature"] = "38-40";
        if (parseInt(d["temperature"]) >= 40) d["temperature"] = "> 40";
    });


    y.domain([0, 100]);
    x0.domain(csv.map(function (d) {
        return d.temperature;
    }));
    x1.domain(["inflammation", "nephritis", "both", "total"]).rangeRoundBands([0, x0.rangeBand()]);

    var data = [];
    setData();

    d3.selectAll(".barChartSettings").on("click", function () {
        setData();
    });

    function setData() {
        var nausea = d3.select('#nausea').property('value');
        var lumber = d3.select('#lumber_pain').property('value');
        var urine = d3.select('#urine_pushing').property('value');
        var mictur = d3.select('#micturition_pains').property('value');
        var burn = d3.select('#burning_itch_swelling').property('value');

        nausea = nausea == 1 ? "yes" : (nausea == 0) ? "no" : "dc";
        lumber = lumber == 1 ? "yes" : (lumber == 0) ? "no" : "dc";
        urine = urine == 1 ? "yes" : (urine == 0) ? "no" : "dc";
        mictur = mictur == 1 ? "yes" : (mictur == 0) ? "no" : "dc";
        burn = burn == 1 ? "yes" : (burn == 0) ? "no" : "dc";

        var temperatures = ["< 38", "38-40", "> 40"];
        data = [];

        for (var temp in temperatures) {
            data[temperatures[temp]] = {
                inflammation_count: 0,
                nephritis_count: 0,
                both_count: 0,
                not_inflammation_count: 0,
                not_nephritis_count: 0,
                not_both_count: 0,
                total_count: 0
            }
        }

        for (var i in csv) {
            var entry = csv[i];
            if (
                ((nausea && entry["nausea"] == nausea) || nausea == "dc") &&
                ((lumber && entry["lumber pain"] == lumber) || lumber == "dc") &&
                ((urine && entry["urine pushing"] == urine) || urine == "dc") &&
                ((mictur && entry["micturition pains"] == mictur) || mictur== "dc") &&
                ((burn && entry["burning/itch/swelling"] == burn) || burn== "dc")
            ) {
                data[entry.temperature].total_count += 1;
                if (entry["inflammation"] == "yes" && entry["nephritis"] == "yes") {
                    data[entry.temperature].both_count += 1;
                }
                else {
                    data[entry.temperature].not_both_count += 1;
                    if (entry["inflammation"] == "yes") {
                        data[entry.temperature].inflammation_count += 1;
                    }
                    else{
                        data[entry.temperature].not_inflammation_count += 1;
                    }
                    if (entry["nephritis"] == "yes") {
                        data[entry.temperature].nephritis_count += 1;   
                    }
                    else{
                        data[entry.temperature].not_nephritis_count += 1;
                    }
                }
            }
        }

        var tempData = [];

        for (var i in data) {
            var entry = data[i];
            tempData.push(
                {
                    group: i,
                    type: "both",
                    count: entry.both_count,
                    count_2 : entry.not_both_count,
                    total_count: entry.total_count
                }
            );
            tempData.push(
                {
                    group: i,
                    type: "nephritis",
                    count: entry.nephritis_count + entry.both_count,
                    count_2:entry.not_nephritis_count,
                    total_count: entry.total_count
                }
            );
            tempData.push(
                {
                    group: i,
                    type: "inflammation",
                    count: entry.inflammation_count + entry.both_count,
                    count_2: entry.not_inflammation_count,
                    total_count: entry.total_count
                }
            );
            tempData.push(
                {
                    group: i,
                    type: "total",
                    count: entry.total_count,
                    count_2: 0,
                    total_count: entry.total_count
                }
            );
        }

        data = [
            {
                group: "< 38",
                counts: tempData.filter(function (d) {
                    return d.group == "< 38"
                })
            },
            {
                group: "38-40",
                counts: tempData.filter(function (d) {
                    return d.group == "38-40"
                })
            },
            {
                group: "> 40",
                counts: tempData.filter(function (d) {
                    return d.group == "> 40"
                })
            }
        ];

        var temp = svg.selectAll(".temp")
            .data(data)
            .attr("class", "temp")
            .attr("transform", function (d) {
                return "translate(" + x0(d.group) + ",0)";
            });


        temp.selectAll("rect")
            .data(function (d) {
                return d.counts;
            })
            .transition()
            .attr("width", x1.rangeBand())
            .attr("x", function (d) {
                return x1(d.type);
            })
            .attr("y", function (d) {
                var percentage = d.total_count > 0 ? d.count / d.total_count * 100 : 0;
                return y(percentage);
            })
            .attr("height", function (d) {
                var percentage = d.total_count > 0 ? d.count / d.total_count * 100 : 0;
                return height - y(percentage);
            })
            .style("fill", function (d) {
                return color(d.type);
            });
    };

    var tooltip = d3.select("body").append("div")
        .attr("class", "tooltip2");

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Percentage of Samples");

    var temp = svg.selectAll(".temp")
        .data(data)
        .enter().append("g")
        .attr("class", "temp")
        .attr("transform", function (d) {
            return "translate(" + x0(d.group) + ",0)";
        });


    temp.selectAll("rect")
        .data(function (d) {
            return d.counts;
        })
        .enter().append("rect")
        .attr("width", x1.rangeBand())
        .attr("x", function (d) {
            return x1(d.type);
        })
        .attr("y", function (d) {
            var percentage = d.total_count > 0 ? d.count / d.total_count * 100 : 0;
            return y(percentage);
        })
        .attr("height", function (d) {
            var percentage = d.total_count > 0 ? d.count / d.total_count * 100 : 0;
            return height - y(percentage);
        })
        .style("fill", function (d) {
            return color(d.type);
        }).on("mouseover", function (d) {
            var pos = d3.event.target.getBoundingClientRect();
            tooltip.attr("style", "left: " + (pos.left) + "; top: " + (pos.top + window.scrollY) + "; opacity: 1;")
                .html(function () {
                    if(d.type == "total"){
                        return d.count + " Samples"
                    }else{
                    return d.count + " Samples<br> " + d.count_2 + " Samples with no" + (d.type=="both"?"t ":" ") + d.type;
                    }
                });
        })
        .on("mouseout", function (d) {
            tooltip.attr("style", "opacity: 0;");
        });

    var legend = svg.selectAll(".legend")
        .data(x1.domain().slice().reverse())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function (d, i) {
            return "translate(120," + i * 20 + ")";
        });

    legend.append("rect")
        .attr("x", width - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

    legend.append("text")
        .attr("x", width - 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function (d) {
            return d;
        });
});
})();
